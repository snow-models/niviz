(function (global) {
  'use strict';

  /**
   * Unit test fixture helper. Provides access to all
   * fixture files in `test/fixtures`.
   *
   * @function fixtures
   * @private
   *
   * @param {String} name The fixture file name.
   * @returns {String} The fixture file's content.
   */

  if (typeof process === 'object') {

    // Fixtures helper implementation for Node.js
    var fs = require('fs');
    var cache = {};

    global.fixtures = function (name) {

      if (!cache.hasOwnProperty(name)) {
        cache[name] =
          fs.readFileSync([__dirname, name].join('/'), { encoding: 'utf-8' });
      }

      return cache[name];
    };

  } else {

    // Fixtures helper implementation for Karma/Browser
    // The __html__ cache is populated by the html2js preprocessor!
    global.fixtures = function (name) {
      return global.__html__[['test/fixtures', name].join('/')];
    };

  }

}((typeof global === 'object') ? global : this));
