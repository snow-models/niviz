// -*- mode: javascript -*-
// vi: set ft=javascript :

module.exports = function (config) {
  'use strict';

  config.set({
    basePath: '..',

    frameworks: ['mocha', 'sinon-chai'],

    files: [
      '.bower/es5-shim/es5-shim.js',
      '.bower/jquery/dist/jquery.js',
      '.bower/moment/moment.js',
      '.bower/raphael/raphael.js',
      '.bower/angular/angular.js',
      '.bower/angular-mocks/angular-mocks.js',

      '.bower/angular-ui-bootstrap/src/alert/alert.js',

      'lib/niviz.js',
      'lib/namespace.js',
      'lib/util.js',
      'lib/synchronizable.js',
      'lib/events.js',
      'lib/config.js',
      'lib/types/generic.js',
      'lib/value.js',
      'lib/values/grainshape.js',
      'lib/values/grainsize.js',
      'lib/values/hardness.js',
      'lib/features.js',
      'lib/graph.js',
      'lib/profile.js',
      'lib/range.js',
      'lib/position.js',
      'lib/meteo.js',
      'lib/station.js',

      'lib/parser.js',
      'lib/parsers/string.js',
      'lib/parsers/xml.js',
      'lib/parsers/pro.js',
      'lib/parsers/caaml.js',
      'lib/parsers/json.js',
      'lib/parsers/smet.js',

      'ui/**/*.js',

      'test/fixtures/helper.js',
      'test/fixtures/**/*.+(pro|smet|caaml|json)',

      'test/ui/**/*.js',
      'test/lib/**/*.js'
    ],

    exclude: [
    ],

    // See: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      '**/*.+(pro|smet|caaml|json)': ['html2js'],
      'lib/**/*.js': ['coverage'],
      'ui/**/*.js': ['coverage']
    },

    // See: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['dots'],

    port: 9876,

    colors: true,

    // config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN ||
    // config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    autoWatch: true,

    // See: https://npmjs.org/browse/keyword/karma-launcher
    browsers: [
      'PhantomJS'
    ],

    singleRun: true
  });
};
