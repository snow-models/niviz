/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Config', function () {
  'use strict';

  var Config  = niviz.Config;
  //var Setting = Config.Setting;

  it('is a constructor function', function () {
    expect(Config).to.be.a('function');
  });

  describe('an instance', function () {
    var config;

    beforeEach(function () {
      config = new Config('foo');
    });

    it('has settings', function () {
      expect(config).to.have.property('settings');
    });

    describe('with setting templates', function () {
      beforeEach(function () {
        config.add([
          { name: 'foo', default: 'bar' },
          { name: 'baz' },
          { name: 'num', type: 'number', default: 42 }
        ]);
      });

      it('has default values', function () {
        expect(config.foo).to.eql('bar');
        expect(config.baz).to.be.undefined;
        expect(config.num).to.eql(42);
      });

      it('has setting accessors', function () {
        config.baz = 'foo';
        expect(config.baz).to.eql('foo');
        expect(config.settings.names.baz.value).to.eql('foo');

        config.num += 1;
        expect(config.num).to.eql(43);
        expect(config.settings.names.num.value).to.eql(43);
      });
    });
  });

});

describe('niviz.Config.Setting', function () {
  'use strict';

  var Config  = niviz.Config;
  var Setting = Config.Setting;

  it('is a constructor function', function () {
    expect(Setting).to.be.a('function');
  });

  describe('an instance', function () {
    var setting;

    beforeEach(function () {
      setting = new Setting({ name: 'x' });
    });

    it('is a string type by default', function () {
      expect(setting.type).to.eql('string');
    });
  });
});
