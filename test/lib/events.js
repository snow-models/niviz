/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('EventEmitter', function () {
  'use strict';

  var EventEmitter = niviz.EventEmitter;
  var emitter;
  var calls;

  beforeEach(function () {
    emitter = new EventEmitter();
    calls   = [];
  });

  it('is a constructor function', function () {
    expect(EventEmitter).to.be.a('function');
  });

  describe('#on(type, listener)', function () {
    it('adds event lister for type', function () {
      emitter.on('foo', function (val) {
        calls.push('one', val);
      });
      emitter.on('foo', function (val) {
        calls.push('two', val);
      });

      emitter.emit('foo', 1);
      emitter.emit('bar', 1);
      emitter.emit('foo', 2);

      expect(calls).to.eql(['one', 1, 'two', 1, 'one', 2, 'two', 2]);
    });
  });

  describe('#off(type, listener)', function () {
     it('removes a listener', function () {

       function one() { calls.push('one'); }
       function two() { calls.push('two'); }

       emitter.on('foo', one);
       emitter.on('foo', two);
       emitter.off('foo', two);

       expect(emitter.listeners('foo')).to.have.length(1);

       emitter.emit('foo');

       expect(calls).to.eql(['one']);
     });

     it('works with #once()', function () {

       function one() { calls.push('one'); }

       emitter.once('foo', one);
       emitter.once('fee', one);

       emitter.off('foo', one);

       emitter.emit('foo');

       expect(calls).to.be.empty;
     });

      it('works when called from an event', function () {
        var called;

        function b () { called = true; }

        emitter.on('baz', function () {
          emitter.off('baz', b);
        });

        emitter.on('baz', b);

        emitter.emit('baz');

        expect(called).to.be.true;

        called = false;
        emitter.emit('baz');

        expect(called).to.be.false;
      });
  });

  describe('#off(type)', function () {
    it('removes all listeners for an event type', function(){

      function one() { calls.push('one'); }
      function two() { calls.push('two'); }

      emitter
        .on('foo', one)
        .on('foo', two);

      emitter.off('foo');

      emitter.emit('foo');
      emitter.emit('foo');

      expect(calls).to.be.empty;
    });
  });

  describe('#off()', function () {
    it('removes all listeners', function () {
      function one() { calls.push('one'); }
      function two() { calls.push('two'); }

      emitter.on('foo', one);
      emitter.on('bar', two);

      emitter.emit('foo');
      emitter.emit('bar');

      emitter.off();

      emitter.emit('foo');
      emitter.emit('bar');

      expect(calls).to.eql(['one', 'two']);
    });
  });

  describe('#once(type, listener)', function (){
    it('adds a one-off listener', function () {
      emitter.once('foo', function (val){
        calls.push('one', val);
      });

      emitter.emit('foo', 1);
      emitter.emit('foo', 2);
      emitter.emit('foo', 3);
      emitter.emit('bar', 1);

      expect(calls).to.eql(['one', 1]);
    });
  });

});
