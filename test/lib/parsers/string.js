/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('StringParser', function () {
  'use strict';

  var Parser       = niviz.Parser;
  var StringParser = Parser.format.string;

  it('is a constructor function', function () {
    expect(StringParser).to.be.a('function');
  });

  it('creates parser instances', function () {
    expect(new StringParser()).to.be.instanceof(Parser);
  });

  describe('#done', function () {
    it('is true by default', function () {
      expect((new StringParser()).done).to.be.true;
    });

    it('is false if there is more data', function () {
      expect((new StringParser('foo')).done).to.be.false;
    });
  });

  describe('#peek()', function () {
    it('returns an empty string by default', function () {
      expect((new StringParser()).peek()).to.eql('');
    });

    it('returns the next k characters', function () {
      var parser = new StringParser('foo\nbar baz');

      expect(parser.peek()).to.eql('f');
      expect(parser.peek(1)).to.eql('f');
      expect(parser.peek(3)).to.eql('foo');

      parser.next();

      expect(parser.peek(3)).to.eql('bar');

      parser.next();
      expect(parser.peek(3)).to.eql('');
    });
  });

  describe('#next()', function () {
    var parser;

    beforeEach(function () {
      parser = new StringParser('foo\nbar\r\nbaz');
    });

    it('returns the next token', function () {
      expect(parser.next()).to.eql('foo');
      expect(parser.next()).to.eql('bar');
      expect(parser.next()).to.eql('baz');
    });

    it('updates the position', function () {
      var original = parser.position;
      var token = parser.next();

      expect(parser.position).to.eql(original + token.length + 1);
    });

    it('returns undefined when done', function () {
      expect(parser.next()).to.not.be.undefined;
      expect(parser.next()).to.not.be.undefined;
      expect(parser.next()).to.not.be.undefined;
      expect(parser.next()).to.be.undefined;
    });
  });
});
