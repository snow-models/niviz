/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Feature', function () {
  'use strict';

  var Feature = niviz.Feature;

  it('is a constructor function', function () {
    expect(Feature).to.be.a('function');
  });

  it('has a name and a symbol dependent on its type', function () {
    var f = new Feature();

    expect(f.type).to.be.undefined;

    f.type = 'thickness';

    expect(f.name).to.eql('Layer thickness');
    expect(f.symbol).to.eql('L');
  });

  describe('JSON Export', function () {
    it('exports only the layers array', function () {
      expect(JSON.stringify(new Feature())).to.eql('{"layers":[]}');
    });
  });

});
