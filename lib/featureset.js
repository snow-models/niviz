/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;
  var Feature    = niviz.Feature;

  /** @module niviz */

  /**
   * A feature of a snow Profile.
   *
   * @class Profile
   * @constructor
   */
  function FeatureSet(type) {

    /**
     * @property type
     * @type String
     */
    this.type = type;

    /**
     * The features stack. Primary features should be at index 0
     *
     * @property elements
     * @type Array
     */
    this.elements = [];
  }

  properties(FeatureSet, {
    /**
     * @property type
     * @type Object
     * @static
     */
    type: {
      value: {}
    }
  });

  properties(FeatureSet.prototype, {
    length: {
      get: function () {
        return this.elements.length;
      }
    },

    /**
     * The values stack. Legacy compatibility
     *
     * @property layers
     * @type Array
     */
    layers: {
      get: function () {
        if (this.elements.length) return this.elements[0].layers;
        return null;
      }
    },

    max: {
      get: function () {
        if (this.elements.length) return this.elements[0].max;
        return Number.NEGATIVE_INFINITY;
      }
    },

    min: {
      get: function () {
        if (this.elements.length) return this.elements[0].min;
        return Number.POSITIVE_INFINITY;
      }
    },

    /**
     * @property definition
     * @type Object
     */
    definition: {
      get: function () { return Feature.type[this.type]; }
    },

    /**
     * @property name
     * @type String
     */
    name: {
      get: function () { return this.definition.name; }
    },

    /**
     * @property symbol
     * @type String
     */
    symbol: {
      get: function () { return this.definition.symbol; }
    },

    /**
     * @property symbol
     * @type String
     */
    unit: {
      get: function () { return this.definition.unit; }
    },

    /**
     * @property top
     * @type Number
     */
    top: {
      get: function () {
        return Math.max.apply(Math, this.elements.filter(function (f) {
          return f;
        }).map(function(f) {
          return f.top;
        })) || 0;
      }
    },

    /**
     * @property bottom
     * @type Number
     */
    bottom: {
      get: function () {
        var b = Math.min.apply(Math, this.elements.filter(function (f) {
          return f;
        }).map(function(f) {
          return f.bottom;
        })); 

        if (b === 0 || b) return b;

        return null;
      }
    },

    /**
     * @property height
     * @type Number
     */
    height: {
      get: function () {
        return Math.max.apply(Math, this.elements.filter(function (f) {
          return f;
        }).map(function(f) {
          return f.height;
        })) || 0;
      }
    },

    /**
     * @property info
     * @type Object
     */
    info: {
      get: function () {
        if (this.elements.length) return this.elements[0].info;
        return {};
      }
    }

  });

  FeatureSet.prototype.toJSON = function () {
    return { type: this.type, elements: this.elements };
  };

  // --- Module Exports ---
  niviz.FeatureSet = FeatureSet;

}(niviz));
