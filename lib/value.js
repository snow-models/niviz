/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;
  var round      = niviz.util.round;

  /** @module niviz */

  /**
   * A Value represents a feature value for
   * a single layer, for example a temparature.
   *
   * @class Value
   * @constructor
   */
  function Value(top, value, bottom) {
    /**
     * @property top
     * @type Number
     */
    this.top = top;

    /**
     * @property value
     * @type Object
     */
    this.value = value;

    /**
     * @property height
     * @type Number
     */
    this.bottom = bottom;
  }


  properties(Value.prototype, {
    /**
     * A string represantation of this value.
     *
     * @property text
     * @type String
     */
    text: {
      get: function () { return this.toString(); }
    },

    /**
     * The numeric value (defaults to `.value`) used for comparisons.
     *
     * @property numeric
     * @type Number
     */
    numeric: {
      get: function () { return Number(this.value); }
    },

    /**
     * The thickness value only makes sense with top and bottom defined
     *
     * @property thickness
     * @type Number
     */
    thickness: {
      get: function () {
        if (this.top !== undefined && this.bottom !== undefined)
          return round(this.top - this.bottom, 10);
      },
      set: function (t) {
        if (t !== undefined && t !== null && this.top !== undefined) {
          this.bottom = round(this.top - t, 10);
        }
      }
    }
  });

  /**
   * A clone that can be used to set new values
   *
   * @method clone
   * @return {Value}
   */
  Value.prototype.clone = function () {
    return new Value(this.top, this.value, this.bottom);
  };

  Value.prototype.toString = function () {
    return String(this.value);
  };

  // --- Module Exports ---
  niviz.Value = Value;

}(niviz));
