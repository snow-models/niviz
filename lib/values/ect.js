/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;
  var properties = Object.defineProperties;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   * Stores extended column test
   *
   * @class ECT
   * @extends Value
   *
   * @constructor
   * @param {Number} [height]
   * @param {Number} [val]
   */
  function ECT (height, val) {

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () { return value; },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    if (val && val.nofailure) height = undefined;

    // Call Value constructor - in turn calls setter above
    Value.call(this, height, val);
  }

  inherits(ECT, Value);

  /**
   * Strips a string 'value' of the initial ECT,
   * extracts number and character
   *
   * @method parse_ect_code
   * @static
   *
   * @param {String} value
   *
   * @return {Object}
   */
  ECT.parse_ect_code = function (value) {
    value = value.replace('ECT', '');
    var search = value.search(/\d/);
    if (search === -1) {
      return {
        value: null,
        value2: null,
        character: value
      };
    } else {
      var character = value.substr(0, search), val = parseInt(value.substr(search));

      return {
        value: val,
        value2: character === 'N' ? null : val,
        character: character
      };
    }

    // throw new Error('Cannot convert string to ECT value: ' + value);
  };

  /**
   * Swiss code ECT codes take the form of 10/20 or 10/np
   * 0 / 0 is equivalent to ECTPV
   * np is equivalent to ECTX
   *
   * @method parse_swiss_code
   * @static
   *
   * @param {String} value
   *
   * @return {Object}
   */
  ECT.parse_swiss_code = function (value) {
    value = value.trim();
    var search = value.search(/\//);
    if (search === -1 && value === 'np') {
      return {
        value1: null,
        value2: null,
        character: 'X'
      };
    } else {
      return {
        value1: value.substr(0, search),
        value2: value.substr(search + 1)
      };
    }

    // throw new Error('Cannot convert string to ECT value: ' + value);
  };

  /**
   * Parse the object or number passed and derive other properties
   *
   * @method parse
   * @param {Object} [value]
   * @return {Number}
   */
  ECT.prototype.parse = function (value) {
    this.nofailure = value.nofailure;
    this.comment   = value.comment || '';

    delete this.swiss;

    if (value.nofailure) {
      value.value = null;

      this.value2 = null;
      this.character = 'X';
      this.top = undefined;
    } else if (value.swiss) {
      if (typeof value.swiss === 'string')
        value.swiss = ECT.parse_swiss_code(value.swiss);

      this.value2 = value.swiss.value2;
      if (value.swiss.character === 'X') {
        this.nofailure = true;
        this.character = 'X';
      } else if (value.swiss.value2 === 'np') {
        this.character = 'N';
      } else if (value.swiss.value2 || value.swiss.value2 === 0) {
        this.character = 'P';
      } else if (value.swiss.value1 === 0 && value.swiss.value2 === 0) {
        this.character = 'PV';
      }

      value.value = value.swiss.value1;
    } else if (typeof value.value === 'string' && !value.value2) {
      // We actually have swiss code if value2 is present
      value = ECT.parse_ect_code(value.value);

      if (value.character === 'X') this.nofailure = true;
      this.character = value.character || '';
    } else { // directly copy values
      this.character = value.character || '';
      this.value2 = value.value2;
    }

    return value.value;
  };

  ECT.prototype.toString = function () {
    if (this.nofailure) return 'ECTX';
    else if (this.character === 'PV') return 'ECTPV';
    else return 'ECT' + this.character + (this.value !== undefined ? this.value : '');
  };

  properties(ECT.prototype, {
    /**
     * Swiss ECT code
     *
     * @property swisscode
     * @type {String}
     */
    swisscode: {
      get: function swisscode$get() {
        if (this.nofailure) return 'np';
        if (this.character === 'PV') return '0/0';
        if (this.character === 'N') return this.value + '/np';

        return this.value + '/' + (this.value2 || this.value);
      }
    }
  });

  ECT.prototype.toJSON = function () {
    var value = pick(this, [ 'character', 'comment', 'nofailure', 'value', 'value2' ]);

    return {
      top: this.top,
      value: value
    };
  };

  // --- Helpers ---

  // --- Module Exports ---
  Value.ECT = ECT;

}(niviz));
