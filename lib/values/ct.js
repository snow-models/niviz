/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   * Stores compression test
   *
   * @class CT
   * @constructor
   * @extends Value
   */
  function CT (height, val) {

    if (!height && height !== 0) height = undefined;

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () { return value; },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    Value.call(this, height, val);
  }

  inherits(CT, Value);

  /**
   * Parse the object or number passed and derive other properties
   *
   * @method parse
   *
   * @param {Object|Number} [value]
   *
   * @return {Number}
   */
  CT.prototype.parse = function (value) {

    if (typeof value !== 'number') {
      this.nofailure = !!value.nofailure;
      this.comment   = value.comment || '';

      if (value.character !== undefined) this.character = value.character;
      else delete this.character;

      if (typeof value.value === 'string') {
        value.value = value.value.trim();

        if (value.value === 'CTE') value = 5;
        else if (value.value === 'CTM') value = 15;
        else if (value.value === 'CTH') value = 25;
        else value = 0;
      } else if (value.value || value.value === 0) {
        value = value.value;
      }
    }

    return value;
  };

  CT.prototype.toString = function () {
    if (this.nofailure) return 'CTNF';
    else return ('CT' + this.value);
  };

  CT.prototype.toJSON = function () {
    var value = pick(this, [ 'character', 'order', 'value', 'comment', 'nofailure']);

    return {
      top: this.top,
      value: value
    };
  };

  // --- Module Exports ---
  Value.CT = CT;

}(niviz));
