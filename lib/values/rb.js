/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   * Stores Rutschblock test
   *
   * @class Rutschblock
   * @extends Value
   *
   * @constructor
   * @param {Number} [height]
   * @param {Number} [value]
   */
  function Rutschblock (height, val) {

    if (!height && height !== 0) {
      val.value = 0;
      height = undefined;
    }

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () { return value; },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    // Call Value constructor
    Value.call(this, height, val);
  }

  inherits(Rutschblock, Value);

  Rutschblock.characters = [
    'Clean', 'Rough', 'Irregular',              //Switzerland
    'Q1', 'Q2', 'Q3',                           //USA
    'SDN', 'SP', 'SC', 'RES', 'PC', 'RP', 'BRK' //Canada
  ];

  /**
   * Strips a string 'value' of the initial RB
   *
   * @method parse_rb_code
   * @static
   *
   * @param {String} value
   *
   * @return {Number}
   */
  Rutschblock.parse_rb_code = function (value) {
    value = parseInt(value.replace('RB', ''));
    if (!isNaN(value)) return value;

    throw new Error('Cannot convert string to Rutschblock value: ' + value);
  };

  /**
   * Parse the object or number passed and derive other properties
   *
   * @method parse
   * @param {Object|Number} [value]
   * @return {Number}
   */
  Rutschblock.prototype.parse = function (value) {

    if (typeof value.value === 'string')
      value.value = Rutschblock.parse_rb_code(value.value);

    this.nofailure   = value.nofailure;
    this.comment     = value.comment;
    this.releasetype = value.releasetype;
    this.character   = value.character;

    if (isNaN(value.value))
      throw new Error('Rutschblock value must be an integer number, was: ' + value.value);

    return value.value;
  };

  Rutschblock.prototype.toString = function () {
    if (this.nofailure) return 'RBNF';
    else return 'RB' + this.value;
  };

  Rutschblock.prototype.toJSON = function () {
    var value = pick(this, [ 'character', 'releasetype', 'comment', 'nofailure', 'value']);

    return {
      top: this.top,
      value: value
    };
  };

  // --- Helpers ---

  // --- Module Exports ---
  Value.Rutschblock = Rutschblock;

}(niviz));
