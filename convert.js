/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* global phantom */
/* eslint-disable no-shadow */

(function (phantom) {
  'use strict';

  window.process = {};

  var console = {log: function () {}};
  var system  = require('system');
  var fs      = require('fs');
  var path    = require('path');

  var manifest;
  var input, inputFilename;
  var type; //type of graph, e.g. SimpleProfile
  var outputType; //SVG, PNG or CAAML

  var options = {
    require: [
      'application.js'
    ]
  };

  var sandbox = function (data, graphType, outputType) {
    try {
      niviz.sync = true;

      var canvas  = $('<svg id="canvas"></svg>');
      canvas.height(900);
      canvas.width(900);

      var station = niviz.parse(data, 'caaml');

      var graph   = niviz.draw(graphType, station, canvas);

      niviz.convert(outputType, graph, function (data, error) {
        window.callPhantom(data);
      });

    } catch (error) {
      window.console.log(error.message);
    }
  };

  function render(callback) {
    var page = require('webpage').create();

    page.settings.userAgent = 'PhantomJS';

    page.onConsoleMessage = function (message) {
      //window.console.log(message);
    };

    page.onCallback = function (data) {

      if (outputType === 'PNG') {
        var outputFile = path.basename(inputFilename);
        outputFile = outputFile.substr(0, outputFile.lastIndexOf('.')) + '.png';

        var b = [].map.call(data, function(v){
          return String.fromCharCode(v);
        }).join("");

        fs.write(outputFile, b, "wb");
        callback('PNG image written to: ' + outputFile);
      } else {
        callback(data);
      }

    };

    page.open('about:blank', function (status) {
      var i, ii, js;

      for (i = 0, ii = options.require.length; i < ii; ++i) {
        js = options.require[i];

        page.injectJs(assetpath(js)) ||
          fail('Failed to inject ', js);
      }

      var svg = page.evaluate(sandbox, input, type, outputType);
    });
  }

  function fail() {
    window.console.error.apply(window.console, arguments);
    phantom.exit();
  }

  function usage() {
    window.console.log('Usage: convert.js <manifest> <file> [SimpleProfile|SLFProfile|StructureProfile] [SVG|PNG|CAAML]');
    phantom.exit();
  }

  function assetpath(name) {
    return [
      manifest.root,
      manifest.assets[name]
    ].join('');
  }

  function parse(args) {
    try {
      if (!fs.isFile(args[0]))
        return fail('Manifest file not found: ', args[0]);

      if (!fs.isFile(args[1]))
        return fail('Input file not found: ', args[1]);

      manifest = JSON.parse(fs.read(args[0]));
      manifest.root = args[0].replace(/[^\/]+$/, '');

      inputFilename = args[1];
      input = fs.read(args[1]);

      type = args[2] || 'SimpleProfile';

      outputType = args[3] || 'SVG';

      return true;

    } catch (error) {
      fail('Failed to parse options: %s', error.message);
    }
  }

  if (system.args.length < 3)
    return usage();

  if (parse(system.args.slice(1)))
    render(function (result) {
      window.console.log(result);
      phantom.exit();
    });


}(phantom));
