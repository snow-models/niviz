/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*eslint strict: [2, "global"] */
'use strict';

var join = require('path').join;
var debug = require('debug')('niviz:server');

var PORT = process.env.PORT || 8000;

var express = require('express');
var app = express();

var Mincer = require('mincer');
var assets = require('./assets')();

app
  .set('views', join(__dirname, 'ui'))
  .set('view engine', 'pug')

  .engine('html', require('ejs').renderFile)

  .use(assets.options.serve, Mincer.createServer(assets.env))

  .use(express.static(join(__dirname, '../public')))

  .use('/resources', express.static('test/fixtures'))

  .use('/img', express.static('assets/img'))

  .get('/simple.html', function (req, res) {
    res.render('simple.pug', { development: true } );
  })

  .get('*', function (req, res) {
    res.render('index.pug', { development: true } );
  })

  .use(function (req, res) {
    debug('%s not found', req.url);

    res
      .status(404)
      .type('txt')
      .send('not found');
  })

  .listen(PORT, function () {
    debug('listening on port %d', PORT);
  });
