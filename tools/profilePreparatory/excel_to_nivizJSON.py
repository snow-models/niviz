#### Python script Excel converter to niViz JSON
#### Authors: Linus Meier, Timo Duerig, Charles Fierz
#### Version 13, 2022-05-14

import json
import re
from datetime import datetime, date, time
from sre_constants import CATEGORY_LOC_NOT_WORD
import pandas as pd

import argparse
import ntpath
import os

import PySimpleGUI as sg
import traceback

################################################################
####  GLOBAL LISTS & DICTIONARIES
## Dictionary of measurements
g_rho_lst = []
g_rho_lst.append({"measurand": "SWE", "headers": ["top","bot","RHOhw"]})
g_rho_lst.append({"measurand": "SWE.1", "headers": ["top.1","bot.1","RHOhw.1"]})
g_rho_lst.append({"measurand": "Density", "headers": ["top.2","bot.2","RHOmeas"]})
g_rho_lst.append({"measurand": "Density.1", "headers": ["top.3","bot.3","RHO"]})
g_rho_lst.append({"measurand": "Density.2", "headers": ["top.4","bot.4","RHO.1"]})
g_lwc_lst = []
g_lwc_lst.append({"measurand": "LWC", "headers": ["top.3","bot.3","LWC"]})
g_lwc_lst.append({"measurand": "LWC.1", "headers": ["top.4","bot.4","LWC.1"]})
g_ssa_lst = []
g_ssa_lst.append({"measurand": "SSA", "headers": ["top.5","bot.5","SSA"]})
g_ts_lst = []
g_ts_lst.append({"measurand": "Snow temperature", "headers": ["Height", "Tsnow"]})
g_types_dict = {'density': g_rho_lst, 'wetness': g_lwc_lst, 'ssa': g_ssa_lst, 'temperature': g_ts_lst}

## Exception lists
g_noComment = ['LWC','LWC.1']
g_methodOther = ['SLF Snow Sensor rl', 'SLF Snow Sensor ssa']
g_pntMeasurand = ['Snow temperature']

################################################################
####  FUNCTIONS
## Converter
def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()

## Functions to fix path OS vulnerability
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)

## Create meta_dict
def meta_dict(meta_df, col_m, pntPrf, methodOther):
    _dict = {}
    _methodIn = str(meta_df.iloc[1,col_m])
    if _methodIn not in methodOther:
        _method = _methodIn
    else:
        _method = 'other'
    _sampler = str(meta_df.iloc[2,col_m])
    _comment = re.sub(r'^nan$', '', str(meta_df.iloc[7,col_m-1]))
    if len(_comment) == 0:
        if len(_methodIn) > 0:
            _comment += _methodIn
        if len(re.sub(r'^nan$', '', _sampler)) > 0:
            _comment += " (" + _sampler + ")"
    if not pntPrf:
        _dict = {"pointprofile": "false","method": _method,"comment": _comment}
    else:
        _dict = {"pointprofile": "true","comment": _comment}

    return _dict

## Create profile structure from df ("layers"; level 4)
def layer_list(prf_df, pntPrf):
    ## match variable names to niViz JSON format
    if not pntPrf:
        prf_df.columns = ["top", "bottom", "value"]
    else:
        prf_df.columns = ["top", "value", "bottom"]
    ## Drop missing values and reset index in case there are spaces (i.e. empty values) in excel / df
    prf_df = prf_df.dropna().reset_index(drop=True)
    ## fix problem with too long floats that niViz does not handle well --> round to 2 decimals
    prf_df = prf_df.astype(float).round(2)
    ## Create one dict per layer
    _values = {}
    for i, row in prf_df.iterrows():
        _values[i] = dict(prf_df.iloc[i,0:3])
    ## put layers into list ---> "layers": [ {"top": ..., "bottom": ..., "value": ...}, { .... }, { .... } ]
    _layers = []
    for k,v in _values.items():
        _layers.append(_values[k])

    return _layers

## Create profile dict
def profile_dict(df, meta_df, measurand, header_lst, pntMeasurand, methodOther, noComment):
    _dict = {}
    _lst_layers = []
    _col_m = meta_df.columns.get_loc(measurand)
    if str(meta_df.iloc[0,_col_m]) == "Yes":
        _pntPrf = False
        _df = df[header_lst].copy()
        if measurand in pntMeasurand:
            _pntPrf = True
            _df["bot"] = _df["Height"]
        if measurand in noComment:
            if measurand == 'LWC':
                _col_m -= 4
            else:
                _col_m -= 1
        _dict_meta = meta_dict(meta_df,_col_m,_pntPrf,methodOther)
        _lst_layers = layer_list(_df,_pntPrf)
    if _lst_layers:
        _dict = {"meta": _dict_meta, "layers": _lst_layers}

    return _dict

################################################################


# GUI settings

### theme
sg.theme('Reddit')

### layout
font = ("Arial", 16)
layout = [[sg.Text('Select Excel-File (.xlsx)')],
          [sg.Input(font=font), sg.FileBrowse(key='-IN-', font=font), sg.Submit(button_text="Convert", font=font)],
          [sg.Output(key='-OUTPUT-', size=(120,30), font=font)]]

### window
window = sg.Window('Excel to niViz JSON converter', layout)

################################################################


# Event Loop

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Convert':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')

            ################################################################
            #####   Start basic app functionality ##########################
            ################################################################

            # Path Set up
            ### full path (dir + file + ext)
            fullpath = values['-IN-']

            ### extract dir from fullpath
            dironly = path_head(fullpath)

            ### extract filename from fullpath
            filename_ext = path_tail(fullpath)
            filename = os.path.splitext(filename_ext)[0]

            ### define output path + file name
            d_out = os.path.join(dironly, filename + ".json")

            ### extract output file name
            filename_new = path_tail(d_out)

            print(f"[INFO] Loading file {filename_ext} ...")
            print("")

            ################################################################


            # Read input from metadata sheet

            sheet = 'Metadata'
            print(f"[INFO] Reading metadata from sheet '{sheet}' ...")
            print("")
            mdata_in = pd.read_excel(values["-IN-"],sheet_name=sheet)
            mdata = mdata_in.iloc[:,2].replace('nan', float("NaN"))

            ### position
            station_name = mdata[2]
            description = mdata[3]
            altitude = round(mdata[4], 0)
            lon = round(mdata[5], 6)
            lat = round(mdata[6], 6)
            ### fixed position values
            uom = "m"
            aspect = "n/a"
            azimuth = None
            angle = None

            ### Date & Observer
            _tstamp = datetime.now().astimezone().isoformat(timespec='minutes')
            if pd.notna(mdata[9]):
                _date = date.fromisoformat(str(mdata[9])[0:10])
                _time = time(0)
                if len(str(mdata[9])) > 10:
                    _time = time.fromisoformat(str(mdata[9])[11:16])
                _tz = _tstamp[-6:]
                _tstamp = datetime.combine(_date, _time).isoformat(timespec='minutes')
                if pd.notna(mdata[10]):
                    if str(mdata[10])[2] == ":":
                        _tz = str(mdata[10])
                    else:
                        _tz = str(mdata[10])[0:3] + ":" + str(mdata[10])[3:5]
                _tstamp += _tz
            obs = mdata[11]
            comment_mdata = mdata[12]

            ### Weather conditions
            precip = mdata.iloc[15]
            sky = mdata.iloc[16]
            ta = mdata.iloc[17]
            vw = mdata.iloc[18]
            dw = mdata.iloc[19]
            comment_weather = mdata.iloc[20]

            ### Surface conditions
            roughness = mdata.iloc[24]
            comment_surface = mdata.iloc[25]

            ### convert "roughness" input to caaml format
            if roughness == "smooth":
                rn = "rsm"
            elif roughness == "wavy":
                rn = "rwa"
            elif roughness == "concave furrows":
                rn = "rcv"
            elif roughness == "convex furrows":
                rn = "rcx"
            elif roughness == "random furrows":
                rn = "rcw"
            else:
                rn = None

            ### Snowpack conditions
            hs = mdata.iloc[27]
            hn24 = mdata.iloc[28]
            comment_snowpack = mdata.iloc[29]

            ### Identification (IDs)
            profile_id = mdata.iloc[32]
            station_id = mdata.iloc[33]
            obs_id = mdata.iloc[34]
            op_id = mdata.iloc[35]

            ################################################################


            # Create metadata dictionaries and subdictionaries
            ## -> only add fields if they contain a value and are not == NaN

            ### Create position dict (level 1)
            position_dict = {}
            position_dict = {"uom":uom,"latitude":lat,"longitude":lon,"altitude":altitude,"angle":angle,"_azimuth":azimuth,"_aspect":aspect,"description":description}

            ### Create info dict (level 2: "profiles" -> "info"), adding only certain values based on condition
            info_dict = {"profile_gmlid":profile_id,"observer_gmlid":obs_id,"operation_gmlid":op_id,"precipitation":"","sky":"","ta":None}
            if pd.notna(precip):
                info_dict["precipitation"] = precip
            if pd.notna(sky):
                info_dict["sky"] = sky
            if pd.notna(ta):
                info_dict["ta"] = ta

            ### Create wind dict (level 3: "info" -> "wind")
            wind_dict = {"speed":None, "dir":""}
            if pd.notna(vw):
                wind_dict["speed"] = vw
            if pd.notna(dw):
                wind_dict["dir"] = dw
            info_dict["wind"] = wind_dict

            info_dict["roughness"] = rn

            ### Create comment dict (level 3: "info" -> "comment")
            com_dict = {}
            if pd.notna(comment_mdata):
                com_dict["metadata"] = comment_mdata
            if pd.notna(comment_weather):
                com_dict["weather"] = comment_weather
            if pd.notna(comment_surface):
                com_dict["surface"] = comment_surface
            if pd.notna(comment_snowpack):
                com_dict["snowpack"] = comment_snowpack
            info_dict["comment"] = com_dict

            ### Create penetration dict (level 3: "info" -> "penetration")
            info_dict["penetration"] = {"ram": "","foot": "","ski": ""}

            if pd.notna(obs):
                info_dict["observer"] = obs
            info_dict["operation"] = "SLF"
            if pd.notna(hn24):
                info_dict["hn24"] = hn24
            if pd.notna(hs):
                info_dict["hs"] = hs

            ### Create slf dict (level 3: "info" -> "slf" and subdict "txt" (level 4))
            slf_subdict = {"default": "","ss": "","rb": "","loc": "","profile": "","drift": "","signals": "","avalanches": "","danger": "","confidential": "","weather": "","remarks": ""}
            slf_dict = {"hasty": False,"profilenr": "","txt":slf_subdict}
            info_dict["slf"] = slf_dict

            print("[INFO] Meta data processing completed ...")
            print("")

            ################################################################


            # Read input from profiles sheet

            sheet = 'Profiles'
            print(f"[INFO] Reading input from sheet '{sheet}' ...")
            print("")

            df = pd.read_excel(values["-IN-"], sheet_name=sheet, header=14)
            df.drop(df.tail(1).index,inplace=True) # Get rid of bottom row of spreadsheet containing strings
            metaInfo_df = pd.read_excel(values["-IN-"], sheet_name=sheet, header=3).head(9)

            print("[INFO] Converting input file to niViz JSON ...")
            print("")

            ### Convert all values to strings [dtype object]. This is needed to write the niViz JSON later on [?].
            df = df.astype('str').replace('nan', float("NaN"))

            ### remove linebreaks and spaces from df header
            df.columns = df.columns.map(lambda x: x.replace('\n','_').replace(' ', '_'))

            ################################################################


            # Create type dictionaries (level 1)
            ##  "<type>" : { "type": "<type>", "elements": [{ "meta": {...}, "layers": [{LAYERS_<type>}, {...}] }, ...}] }

            niViz_dict = {}
            _types_dict = {"date":_tstamp, "info":info_dict}
            ### Loop over types
            for _type in g_types_dict.keys():
                _elements_lst = []
                _type_dict ={}
                _profiles_dict = {}
                ### Loop over measurands
                for d in g_types_dict[_type]:
                    _profile_dict = {}
                    m = d['measurand']
                    if 'LWC' in m and not _elements_lst:
                        _elements_lst = [None]
                    h = d['headers']
                    _profile_dict = profile_dict(df,metaInfo_df,m,h,g_pntMeasurand,g_methodOther,g_noComment)
                    if _profile_dict:
                       _elements_lst.append(_profile_dict)
                if _elements_lst:
                    _type_dict = {"type":_type, "elements":_elements_lst}
                    _types_dict[_type] = _type_dict
            profiles_lst = [_types_dict]
            niViz_dict = {"id":station_id, "name":station_name, "position":position_dict, "profiles":profiles_lst, "creationdate":datetime.now().astimezone().isoformat(timespec='minutes')}

            ################################################################


            # Dump niViz dict to niViz JSON and save to disk

            ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
            with open(d_out, "w") as json_mile:
                json.dump(niViz_dict, json_mile, default = myconverter)

            ################################################################


            # Done, give feedack to user

            print("[INFO] Done! :-)")
            print("")
            print(f'[INFO] Wrote niViz JSON file: "{d_out}"')
            print("")
            print(f'[INFO] To proceed, simply import "{filename_new}" on run.niviz.org')
            print("")

            ################################################################
            #####   End basic app functionality ############################
            ################################################################


        # Error handling

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ooops, an error occured =( \n\n Here is the info:', e, tb)
            window['-OUTPUT-'].update('')
