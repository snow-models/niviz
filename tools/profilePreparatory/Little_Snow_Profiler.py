
#### Python script Excel converter to niViz JSON and More
#### Authors: Linus Meier, Timo Duerig, Charles Fierz, Gian-Andri Morf
#### Version 14, 2023-02-10
#### Build with python verion 3.7

## Parts that could be improved:
## - Implementation of a function: caaml to .json and vise versa
## - Implementation of a function that adjust the snowhight according to the newly imported measurements

### excel and json libraries
import json
import string
import re
from datetime import datetime, date, time
from sre_constants import CATEGORY_LOC_NOT_WORD
import pandas as pd
import numpy as np
import csv
import argparse
import ntpath
import os

# Gui and programm architecture libraries
import PySimpleGUI as sg
import traceback

### smp tool libraries
import matplotlib.pyplot as plt
import matplotlib.axes as axs
import glob
import matplotlib.transforms
from matplotlib import ticker, cm
from matplotlib.colors import LogNorm

################################################################
####  GLOBAL LISTS & DICTIONARIES
## Dictionary of measurements
g_rho_lst = []
g_rho_lst.append({"measurand": "SWE", "headers": ["top","bot","RHOhw"]})
g_rho_lst.append({"measurand": "SWE.1", "headers": ["top.1","bot.1","RHOhw.1"]})
g_rho_lst.append({"measurand": "Density", "headers": ["top.2","bot.2","RHOmeas"]})
g_rho_lst.append({"measurand": "Density.1", "headers": ["top.3","bot.3","RHOmeas.1"]})
g_rho_lst.append({"measurand": "Density.2", "headers": ["top.4","bot.4","RHO"]})
g_rho_lst.append({"measurand": "Density.3", "headers": ["top.5","bot.5","RHO.1"]})
g_lwc_lst = []
g_lwc_lst.append({"measurand": "LWC", "headers": ["top.3","bot.3","LWC"]})
g_lwc_lst.append({"measurand": "LWC.1", "headers": ["top.4","bot.4","LWC.1"]})
g_ssa_lst = []
g_ssa_lst.append({"measurand": "SSA", "headers": ["top.5","bot.5","SSA"]})
g_ssa_lst.append({"measurand": "SSA.1", "headers": ["top.6","bot.6","SSA.1"]})
g_ts_lst = []
g_ts_lst.append({"measurand": "Snow temperature", "headers": ["Height", "Tsnow"]})
g_ts_lst.append({"measurand": "Snow temperature.1", "headers": ["Height", "Tsnow.1"]})
g_types_dict = {'density': g_rho_lst, 'wetness': g_lwc_lst, 'ssa': g_ssa_lst, 'temperature': g_ts_lst}

## Exception lists
g_noComment = ['LWC','LWC.1']
g_methodOther = ['SLF Snow Sensor rl', 'SLF Snow Sensor ssa']
g_pntMeasurand = ['Snow temperature','Snow temperature.1']

################################################################
####  FUNCTIONS
## Converter
def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()

## Functions to fix path OS vulnerability
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)

################################################################
### Function: Create meta_dict
################################################################
def meta_dict(meta_df, col_m, pntPrf, methodOther):
    _dict = {}
    _methodIn = str(meta_df.iloc[1,col_m])
    if _methodIn not in methodOther:
        _method = _methodIn
    else:
        _method = 'other'
    _sampler = str(meta_df.iloc[2,col_m])
    _comment = re.sub(r'^nan$', '', str(meta_df.iloc[7,col_m-1]))
    if len(_comment) == 0:
        if len(_methodIn) > 0:
            _comment += _methodIn
        if len(re.sub(r'^nan$', '', _sampler)) > 0:
            _comment += " (" + _sampler + ")"
    if not pntPrf:
        _dict = {"pointprofile": "false","method": _method,"comment": _comment}
    else:
        _dict = {"pointprofile": "true","comment": _comment}

    return _dict

################################################################
### Function: Create profile structure from df ("layers"; level 4)
################################################################
def layer_list(prf_df, pntPrf):
    ## match variable names to niViz JSON format
    if not pntPrf:
        prf_df.columns = ["top", "bottom", "value"]
    else:
        prf_df.columns = ["top", "value", "bottom"]
    ## Drop missing values and reset index in case there are spaces (i.e. empty values) in excel / df
    prf_df = prf_df.dropna().reset_index(drop=True)
    ## fix problem with too long floats that niViz does not handle well --> round to 2 decimals
    prf_df = prf_df.astype(float).round(2)
    ## Create one dict per layer
    _values = {}
    for i, row in prf_df.iterrows():
        _values[i] = dict(prf_df.iloc[i,0:3])
    ## put layers into list ---> "layers": [ {"top": ..., "bottom": ..., "value": ...}, { .... }, { .... } ]
    _layers = []
    for k,v in _values.items():
        _layers.append(_values[k])

    return _layers

################################################################
### Function: Create profile dict
################################################################
def profile_dict(df, meta_df, measurand, header_lst, pntMeasurand, methodOther, noComment):
    _dict = {}
    _lst_layers = []
    _col_m = meta_df.columns.get_loc(measurand)
    if str(meta_df.iloc[0,_col_m]) == "Yes":
        _pntPrf = False
        _df = df[header_lst].copy()
        if measurand in pntMeasurand:
            _pntPrf = True
            _df["bot"] = _df["Height"]
        if measurand in noComment:
            if measurand == 'LWC':
                _col_m -= 4
            else:
                _col_m -= 1
        _dict_meta = meta_dict(meta_df,_col_m,_pntPrf,methodOther)
        _lst_layers = layer_list(_df,_pntPrf)
    if _lst_layers:
        _dict = {"meta": _dict_meta, "layers": _lst_layers}
    return _dict

################################################################
### Function: Get profile out of a measurement sub_part of a json file
################################################################
def get_json_sub_part(sub_source_file, key_word):
    key_json_sub_part = ""
    with open(sub_source_file) as f:
        sub_source_data = json.load(f)
    #make string aut of json
    sub_source_data_str = str(sub_source_data)
    # find the position of the key_word in the json file
    index = sub_source_data_str.find(key_word)-46 ## 44 when structure like "{"meta":{"pointprofile":"false","method":"Snow Cutter"..."
    print(index)
    i = 0
    if sub_source_data_str[index] == "{":
        while i < 2: #because of the two "]" brackets for each measurement set
            if sub_source_data_str[index] == chr(39):
                key_json_sub_part = key_json_sub_part + chr(34)#because the json format demands it
            else:
                key_json_sub_part = key_json_sub_part + sub_source_data_str[index]
            if sub_source_data_str[index] == "]":
                i += 1
            if i == 1:
                key_json_sub_part =  key_json_sub_part + "}"
                break
            index = index + 1
    return key_json_sub_part


################################################################
### Function: Migrate a sub part (in json) into an existing main json file to add a measurement into a existing snowprofile
################################################################
def migrate_sub_part_into_json(sub_part, key_word, main_json):
    # disitinguish the phisical unit of measurement (you can add different types here)
    measurement_unit=""
    if key_word == 'Snow Cutter' or key_word == 'Snow Tube' or key_word == 'density' or key_word == 'Snow Cylinder' or key_word == 'Denoth probe':
        measurement_unit = 'density'
    elif key_word == 'Ice Cube' or key_word == 'ssa' or key_word == 'SLF':
        measurement_unit = 'ssa'
    elif key_word == 'Temperature':
        measurement_unit = 'temperature'
    #especially for smp hardness/force measurments
    elif key_word == 'smp':
        measurement_unit= 'smp'

    # load main_json
    with open(main_json) as ff:
        #destinguish if json from excel or .json
        sub_source_data = json.load(ff)
    #make string aut of json
    sub_source_data_str = str(sub_source_data)

    #look in the main json if there is already a measurement to this measurement measurement_unit
    if sub_source_data_str.find(measurement_unit) == -1:
        # find the position of the last element in the json filename
        i = 0
        index = len(sub_source_data_str)
        # search for the end of the json and insert a new measurement type with measurement proifle date from the sub part
        while i < 2:
            if sub_source_data_str[index-1] == "}":
                i = i + 1
            index = index -1

        # migrate sub part at index position
        head_string = sub_source_data_str[:index]
        tail_string = sub_source_data_str[(index+1):]
        merged_source_data = head_string + ", " + chr(34) + measurement_unit + chr(34)+ ": {" + chr(34) + "type" + chr(34) + ":" + chr(34) + key_word + chr(34) + "," +  chr(34) + "elements" + chr(34) + ": [" + sub_part + "]}}" + tail_string

        # make string json compatible regarding syntax
        merged_source_data = merged_source_data.replace(chr(39), chr(34))
        merged_source_data = merged_source_data.replace("nan","null")
        merged_source_data = merged_source_data.replace("None","null")
        merged_source_data = merged_source_data.replace("False","false")
    else:
        # find the position of the measurement_unit in the json file
        index = sub_source_data_str.find(measurement_unit)
        i = 0
        while i < 1: #because of the two "]" brackets for each measurement set
            index = index + 1
            if sub_source_data_str[index] == "[":
                break

        # migrate sub part at index position
        head_string = sub_source_data_str[:index]
        tail_string = sub_source_data_str[(index+1):]
        merged_source_data = head_string + "[" + sub_part + ", " + tail_string

        # make string json compatible regarding syntax
        merged_source_data = merged_source_data.replace(chr(39), chr(34))
        merged_source_data = merged_source_data.replace("nan","null")
        merged_source_data = merged_source_data.replace("None","null")
        merged_source_data = merged_source_data.replace("False","false")
    return merged_source_data

################################################################
### Function: Update timestamp in json
################################################################
def update_timestamp(json_string):
    if json_string.find("lastedited") == True:
        index = json_string.find("lastedited")-1
        usefull_json = json_string[:index]
        index2 = json_string.find("creationdate")-1
        usefull_ending = json_string[index2:]
        usefull_json = usefull_json + chr(34) + "lastedited" + chr(34) + ":" + chr(34) + str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + chr(34) + chr(44) + usefull_ending #chr(34) = "  chr(44) = ,
    else:
        index = json_string.find("creationdate")-1
        usefull_json = json_string[:index]
        usefull_ending = json_string[index:]
        usefull_json = usefull_json + chr(34) + "lastedited" + chr(34) + ":" + chr(34) + str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + chr(34) +  chr(44) + usefull_ending #chr(34) = "
    return usefull_json

################################################################
### Function: fix path OS vulnerability
################################################################
def path_tail(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def path_head(path):
    head, tail = ntpath.split(path)
    return head or ntpath.basename(tail)


# GUI settings

### theme
################################################################
sg.theme('DarkBlue3')

### layout
################################################################
header = ("Arial", 30)
font_gen = ("Arial", 18)
font_small = ("Arial", 12)
sg.popup_quick_message('Hang on for a moment, this will take a bit to create....', auto_close=True, non_blocking=True, font='Default 18')
sg.set_options(element_padding=(0, 0))

### menu bar
################################################################
menu_def = [['File', ['Open Profile From .csv', 'Exit']],
            ['Help', 'About...'], ]

### input table dimensions
################################################################
MAX_ROWS = 20
MAX_COL = 3
columm_layout =  [[sg.InputText(size=(14, 1), pad=(1,1),border_width=0, justification='center', background_color='lightblue', do_not_clear=False, key=(i, j)) for j in range(MAX_COL)] for i in range(MAX_ROWS)]

################################################################

col1 =  [
          [sg.Text('Excel to .json',font=font_gen, text_color='lightblue')],
          [sg.Text('.xlsx template chart', font=font_small, text_color='black')],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-', font=font_small, button_color="black")],
          [sg.Submit(button_text="Convert Excel to .json", font=font_small, button_color ="grey")],
          [sg.Text('     ',font=(0.1))],
          [sg.HorizontalSeparator()],
          [sg.Text('     ',font=(0.1))],
          [sg.Text('Process SMP Data',font=font_gen, text_color='lightblue')],
          [sg.Text('scelect which SMP to upload', font=font_small)],
          [sg.InputCombo(('hardness','density','ssa'), key='SMP-keywords', font=font_small)],
          [sg.Text('',font=(0.1))],
          [sg.Text('select main .json file to which smp profile should be added', font=font_small, text_color='black')],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-main-json-3', font=font_small, button_color ="black")],
          [sg.Text('select file (ABOA0xxx_derivatives.csv)', font=font_small)],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-smp-derivatives', font=font_small, button_color ="grey")],
          [sg.Submit(button_text="SMP to Profile",font=font_small, button_color="grey"), sg.Text(' ',font=(0.1)), sg.Submit(button_text="Create SMP Plot",font=font_small, button_color="grey")],
          [sg.Text('     ',font=(0.1))],
          [sg.HorizontalSeparator()],
          [sg.Text('     ',font=(0.1))],
          [sg.Text('Integrate measurement in .json Profile',font=font_gen, text_color='lightblue')],
          [sg.Text('select measurement type and upload .json file', font=font_small)],
          [sg.InputCombo(('Denoth probe','Ice Cube','Snow Cylinder','Snow Tube','Snow Cutter','Temperature', 'ssa'), key='-IN-measurement-type', font=font_small)],
          [sg.Text('     ',font=(0.01))],
          [sg.Text('select main .json file to which profile should be added', font=font_small, text_color='black')],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-main-json-2', font=font_small, button_color ="black")],
          [sg.Text('select .json file containing the measurement, which should be added', font=font_small)],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-external-json-', font=font_small, button_color ="grey")],
          [sg.Submit(button_text="Add File To Profile", font=font_small, button_color="grey")],
        ]

          # input table layout
col2 =  [
          [sg.Text('Input Table', font=font_gen, text_color='lightblue')],
          [sg.Text('select measurement type and upload .json file', font=font_small)],
          [sg.InputCombo(('Denoth probe', 'Snow Cylinder', 'Ice Cube','Snow Tube','Snow Cutter','Temperature','ssa'), key='table_keyword', font=font_small)],
          [sg.Text('     ',font=(0.1))],
          [sg.Text('write a comment to the measurement', font=font_small)],
          [sg.Input(font=font_small, size=(30,20), key ='text_comment')],
          [sg.Text('select location where to save the new .json file', font=font_small)],
          [sg.Input(font=font_small, size=(30,20)), sg.FolderBrowse(key='-Out-table-json-', font=font_small, button_color ="grey")],
          [sg.Text('select a main .json here', font=font_small, text_color='black')],
          [sg.Input(font=font_small, size=(30,20)), sg.FileBrowse(key='-IN-main-json-4', font=font_small, button_color ="grey")],
          [sg.Text('  ',font=font_small)],
          [sg.Text('Top', size=(14, 0), pad=(1,0),border_width=0, justification='center'), sg.Text('Bottom', size=(14, 0), pad=(1,0),border_width=0, justification='center'), sg.Text('Val',size=(14, 0), pad=(1,0),border_width=0, justification='center')],
          [sg.Col(columm_layout, key='input_table')],
          [sg.Text('     ',font=font_small)],
          [sg.Submit(button_text="Table -> Json", font=font_small, button_color="grey"), sg.Text(' '), sg.Submit(button_text="Values -> Profile", font=font_small, button_color="grey"), sg.Text(' '), sg.Submit(button_text="Clear", font=font_small, button_color="grey")]
        ]

#layout of PySimpleGUI Window
################################################################
layout = [
          [sg.Menu(menu_def)],
          [sg.Column(col1, element_justification='left')] + [sg.Text('               ')] + [sg.Column(col2, element_justification='left')],
          [sg.Text('     ',font=(12))],
          [sg.Output(key='-OUTPUT-', font=font_small, text_color='black', size=(150,50), background_color='lightgrey')]
         ]

### top bar in the window
window = sg.Window('Little Snow Profiler', layout,  return_keyboard_events=True, resizable=True, size=(730,750))

# Event Loop
################################################################
while True:
    event, values = window.read()
    if event in (sg.WIN_CLOSED, 'Exit'):
         break

################################################################
#####   Start basic app functionality (Excel to Json)###########
################################################################

    if event == 'Convert Excel to .json':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')

            # Path Set up
            ### full path (dir + file + ext)
            fullpath = values['-IN-']

            ### extract dir from fullpath
            dironly = path_head(fullpath)

            ### extract filename from fullpath
            filename_ext = path_tail(fullpath)
            filename = os.path.splitext(filename_ext)[0]

            ### define output path + file name
            d_out = os.path.join(dironly, filename + ".json")

            ### extract output file name
            filename_new = path_tail(d_out)

            print(f"[INFO] Loading file {filename_ext} ...")
            print("")

            ################################################################


            # Read input from metadata sheet

            sheet = 'Metadata'
            print(f"[INFO] Reading metadata from sheet '{sheet}' ...")
            print("")
            mdata_in = pd.read_excel(values["-IN-"],sheet_name=sheet, engine='openpyxl')
            mdata = mdata_in.iloc[:,2].replace('nan', float("NaN"))

            ### position
            station_name = mdata[2]
            description = mdata[3]
            altitude = round(mdata[4], 0)
            lon = round(mdata[5], 6)
            lat = round(mdata[6], 6)
            ### fixed position values
            uom = "m"
            aspect = "n/a"
            azimuth = None
            angle = None

            ### Date & Observer
            _tstamp = datetime.now().astimezone().isoformat(timespec='minutes')
            if pd.notna(mdata[9]):
                _date = date.fromisoformat(str(mdata[9])[0:10])
                _time = time(0)
                if len(str(mdata[9])) > 10:
                    _time = time.fromisoformat(str(mdata[9])[11:16])
                _tz = _tstamp[-6:]
                _tstamp = datetime.combine(_date, _time).isoformat(timespec='minutes')
                if pd.notna(mdata[10]):
                    if str(mdata[10])[2] == ":":
                        _tz = str(mdata[10])
                    else:
                        _tz = str(mdata[10])[0:3] + ":" + str(mdata[10])[3:5]
                _tstamp += _tz
            obs = mdata[11]
            comment_mdata = mdata[12]

            ### Weather conditions
            precip = mdata.iloc[15]
            sky = mdata.iloc[16]
            ta = mdata.iloc[17]
            vw = mdata.iloc[18]
            dw = mdata.iloc[19]
            comment_weather = mdata.iloc[20]

            ### Surface conditions
            roughness = mdata.iloc[24]
            comment_surface = mdata.iloc[25]

            ### convert "roughness" input to caaml format
            if roughness == "smooth":
                rn = "rsm"
            elif roughness == "wavy":
                rn = "rwa"
            elif roughness == "concave furrows":
                rn = "rcv"
            elif roughness == "convex furrows":
                rn = "rcx"
            elif roughness == "random furrows":
                rn = "rcw"
            else:
                rn = None

            ### Snowpack conditions
            hs = mdata.iloc[27]
            hn24 = mdata.iloc[28]
            comment_snowpack = mdata.iloc[29]

            ### Identification (IDs)
            profile_id = mdata.iloc[32]
            station_id = mdata.iloc[33]
            obs_id = mdata.iloc[34]
            op_id = mdata.iloc[35]

            ################################################################


            # Create metadata dictionaries and subdictionaries
            ## -> only add fields if they contain a value and are not == NaN

            ### Create position dict (level 1)
            position_dict = {}
            position_dict = {"uom":uom,"latitude":lat,"longitude":lon,"altitude":altitude,"angle":angle,"_azimuth":azimuth,"_aspect":aspect,"description":description}

            ### Create info dict (level 2: "profiles" -> "info"), adding only certain values based on condition
            info_dict = {"profile_gmlid":profile_id,"observer_gmlid":obs_id,"operation_gmlid":op_id,"precipitation":"","sky":"","ta":None}
            if pd.notna(precip):
                info_dict["precipitation"] = precip
            if pd.notna(sky):
                info_dict["sky"] = sky
            if pd.notna(ta):
                info_dict["ta"] = ta

            ### Create wind dict (level 3: "info" -> "wind")
            wind_dict = {"speed":None, "dir":""}
            if pd.notna(vw):
                wind_dict["speed"] = vw
            if pd.notna(dw):
                wind_dict["dir"] = dw
            info_dict["wind"] = wind_dict

            info_dict["roughness"] = rn

            ### Create comment dict (level 3: "info" -> "comment")
            com_dict = {}
            if pd.notna(comment_mdata):
                com_dict["metadata"] = comment_mdata
            if pd.notna(comment_weather):
                com_dict["weather"] = comment_weather
            if pd.notna(comment_surface):
                com_dict["surface"] = comment_surface
            if pd.notna(comment_snowpack):
                com_dict["snowpack"] = comment_snowpack
            info_dict["comment"] = com_dict

            ### Create penetration dict (level 3: "info" -> "penetration")
            info_dict["penetration"] = {"ram": "","foot": "","ski": ""}

            if pd.notna(obs):
                info_dict["observer"] = obs
            info_dict["operation"] = "SLF"
            if pd.notna(hn24):
                info_dict["hn24"] = hn24
            if pd.notna(hs):
                info_dict["hs"] = hs

            ### Create slf dict (level 3: "info" -> "slf" and subdict "txt" (level 4))
            slf_subdict = {"default": "","ss": "","rb": "","loc": "","profile": "","drift": "","signals": "","avalanches": "","danger": "","confidential": "","weather": "","remarks": ""}
            slf_dict = {"hasty": False,"profilenr": "","txt":slf_subdict}
            info_dict["slf"] = slf_dict

            print("[INFO] Meta data processing completed ...")
            print("")

            ################################################################


            # Read input from profiles sheet

            sheet = 'Profiles'
            print(f"[INFO] Reading input from sheet '{sheet}' ...")
            print("")

            df = pd.read_excel(values["-IN-"], sheet_name=sheet, header=14, engine='openpyxl')
            df.drop(df.tail(1).index,inplace=True) # Get rid of bottom row of spreadsheet containing strings
            metaInfo_df = pd.read_excel(values["-IN-"], sheet_name=sheet, header=3, engine='openpyxl').head(9)

            print("[INFO] Converting input file to niViz JSON ...")
            print("")

            ### Convert all values to strings [dtype object]. This is needed to write the niViz JSON later on [?].
            df = df.astype('str').replace('nan', float("NaN"))

            ### remove linebreaks and spaces from df header
            df.columns = df.columns.map(lambda x: x.replace('\n','_').replace(' ', '_'))

            ################################################################


            # Create type dictionaries (level 1)
            ##  "<type>" : { "type": "<type>", "elements": [{ "meta": {...}, "layers": [{LAYERS_<type>}, {...}] }, ...}] }

            niViz_dict = {}
            _types_dict = {"date":_tstamp, "info":info_dict}
            ### Loop over types
            for _type in g_types_dict.keys():
                _elements_lst = []
                _type_dict ={}
                _profiles_dict = {}
                ### Loop over measurands
                for d in g_types_dict[_type]:
                    _profile_dict = {}
                    m = d['measurand']
                    if 'LWC' in m and not _elements_lst:
                        _elements_lst = [None]
                    h = d['headers']
                    _profile_dict = profile_dict(df,metaInfo_df,m,h,g_pntMeasurand,g_methodOther,g_noComment)
                    if _profile_dict:
                       _elements_lst.append(_profile_dict)
                if _elements_lst:
                    _type_dict = {"type":_type, "elements":_elements_lst}
                    _types_dict[_type] = _type_dict
            profiles_lst = [_types_dict]
            niViz_dict = {"id":station_id, "name":station_name, "position":position_dict, "profiles":profiles_lst, "creationdate":datetime.now().astimezone().isoformat(timespec='minutes')}

            ################################################################


            # Dump niViz dict to niViz JSON and save to disk

            ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
            with open(d_out, "w") as json_mile:
                json.dump(niViz_dict, json_mile, default = myconverter)

            # Enter .json path in brows box to proceed :)
            values['-IN-'] = d_out
            values.update()

            ################################################################

            # Done, give feedack to user

            print("[INFO] Done! :-)")
            print("")
            print(f'[INFO] Wrote niViz JSON file: "{d_out}"')
            print("")
            print(f'[INFO] To proceed, simply import "{filename_new}" on run.niviz.org')
            print("")

        # Error handling
        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ooops, an error occured =( \n\n Here is the info:', e, tb)
            window['-OUTPUT-'].update('')


################################################################
#################   Input Table to Profile  ####################
################################################################

    if event == 'About...':
            sg.popup('Demo of table capabilities')

    if event == 'Open Profile From .csv':
            filename = sg.popup_get_file(
                'filename to open', no_window=True, file_types=(("CSV Files", "*.csv"),))
            # --- populate table with file contents --- #
            if filename is not None:
                with open(filename, "r") as infile:
                    reader = csv.reader(infile)
                    try:
                        # read everything else into a list of rows
                        data = list(reader)
                    except:
                        sg.popup_error('Error reading file')
                        continue
                # clear the table
                [window[(i, j)].update('') for j in range(MAX_COL)
                 for i in range(MAX_ROWS)]

                for i, row in enumerate(data):
                    for j, item in enumerate(row):
                        location = (i, j)
                        try: # try the best we can at reading and filling the table
                            target_element = window[location]
                            new_value = item
                            if target_element is not None and new_value != '':
                                target_element.update(new_value)
                        except:
                            pass

################################################################
#####   Start basic app functionality (Add Profile to Json)#####
################################################################

    if event == 'Add File to Profile':
        try:
            ### clear printed output
            window.FindElement('-OUTPUT-').Update('')

            # Path Set up
            # check variables #reset checkmarks
            check1 = False
            check2 = False
            check3 = False

            # full path of main .json if existing (dir + file + ext)
            if  values['-IN-main-json-2'] == None:
                print(f"[INFO] No, main Json is defined! \n Please add a main excle and convert it to json or brows for an external main json ;)")
                print("")
            else:
                fullpath_json_main = values['-IN-main-json-2']
                filename_json_main = path_tail(fullpath_json_main)
                print(f"[INFO] Loading main json file {filename_json_main} ...")
                print("")
                check1 = True

            ### full external measurement path from
            if values['-IN-external-json-']=="":
                print(f"[INFO] No additional measurement file is defined! \n Please browse after a file ;)")
                print("")
            else:
                fullpath_extern_json = values['-IN-external-json-'] #browsed other main json file
                filename_extern_json = path_tail(fullpath_extern_json)
                print(f"[INFO] Loading additional measurement file {filename_extern_json} ...")
                print("")
                check2 = True

            ################################################################

            # Select measurement type
            if values['-IN-measurement-type']=="":
                print(f"[INFO] Additional measurement type not yet selected. \n Please use the drope down menue to select a type ;)")
                print("")
            else:
                selected_measurement = values['-IN-measurement-type']
                print(f"[INFO] {selected_measurement} has been selected.")
                print("")
                check3 = True

            ################################################################

            # Extrating the measurement data
            if (check1 and check2 and check3):

                #get json code of selected_measurement located in the external json file
                sub_part =  get_json_sub_part(fullpath_extern_json, selected_measurement)
                #migrate into the the existing main json_mile
                merged_json = migrate_sub_part_into_json(sub_part, selected_measurement, fullpath_json_main)

                ## update timestamp in newly merged json
                merged_json = update_timestamp(merged_json)

                # dump merged_json and save to disk
                ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
                with open(filename_json_main, "w") as json_merge:
                    merged_json = json.loads(merged_json)
                    json.dump(merged_json, json_merge, default = myconverter)
                ################################################################


        # Error handling
        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ooops, an error occured =( \n\n Here is the info:', e, tb)
            window['-OUTPUT-'].update('')


################################################################
##################        SMP Integrating       ################
################################################################

    if event == 'SMP to Profile':
        try:
                ### clear printed output
                window.FindElement('-OUTPUT-').Update('')

                def smp_to_json(smp_depth, smp, unit, measurement, comment, smp_steps):
                    if measurement == "smp":
                        values = ""
                        head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "true" + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                        tail = "}]}"
                        values = ""
                        for i in range(0, len(smp_depth)):
                            values = values + "{" + chr(34) + "value" + chr(34) + ":{" + chr(34) + "depth" + chr(34) + ":" + str(smp_depth.iloc[i]) + "," + chr(34) + "value" + chr(34) + ":" + str(smp.iloc[i]) + "}" + "},"
                            i = i + 1
                        values = str(values[0:(len(values)-2)])
                        smp_profile = head + values + tail
                    # differntiate because it is not a pointprofile
                    else:
                        head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "false" + chr(34) + ", "  + chr(34) +  "method" + chr(34) + ":" + chr(34) + "other" + chr(34) + ", "  + chr(34) +  "comment" + chr(34) + ":" + chr(34) + comment + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                        tail = "}]}"
                        value = 0
                        values = ""
                        i = 5 # counting from top to bottom of snowcover (bound of 5*0.005cm)
                        # make an averaged non pointprofile
                        while i <= (len(smp_depth)-2*smp_steps): # (2*smp_steps) that we have at least some space in the end
                            for ii in range(0, smp_steps):
                                index = i + ii
                                print(index)
                                # make average of the values
                                value = value +(1/smp_steps)*(smp.iloc[index])
                            # round value
                            value=np.round(value,1)
                            index2 = i
                            index3 = i + smp_steps
                            top = np.round(smp_depth.iloc[len(smp_depth)-index2],1)
                            bot = np.round(smp_depth.iloc[len(smp_depth)-index3],1)
                            values = values + "{" + chr(34) + "top" + chr(34) + ":" + str(top) +  "," + chr(34) + "bottom" + chr(34) + ":" + str(bot) + "," + chr(34) + "value" + chr(34) + ":" + str(value) + "},"
                            value = 0
                            i = i + smp_steps
                        values = str(values[0:(len(values)-2)])
                        smp_profile = head + values + tail
                    return smp_profile

                # check variables #reset checkmarks
                check1 = False
                check2 = False
                check3 = False
                # full path of main .json if existing (dir + file + ext)
                if  values['-IN-main-json-3'] == "":
                    print(f"[INFO] No, main Json is defined! \n Please add a main excle and convert it to json (first part) and browes for the main json ;)")
                    print("")
                else:
                    fullpath_json_main = values['-IN-main-json-3']
                    filename_json_main = path_tail(fullpath_json_main)
                    print(f"[INFO] Loading main json file {filename_json_main} ...")
                    print("")
                    check1 = True

                # full path (dir + file + ext)
                if values['-IN-smp-derivatives'] == "":
                    print(f"[INFO] No smp file is defined! \n Please browse after a file ;)")
                    print("")
                else:
                    fullpath = str(values["-IN-smp-derivatives"])
                    filename_smp = path_tail(fullpath)
                    print(f"[INFO] Loading smp  file {filename_smp} ...")
                    print("")
                    check2 = True

                ################################################################

                # Select measurement type
                if values["SMP-keywords"] == "":
                    print(f"[INFO] SMP measurement type not yet selected. \n Please use the drope down menue to select a type ;)")
                    print("")
                else:
                    measurement_type = str(values["SMP-keywords"])
                    print(f"[INFO] {measurement_type} has been selected.")
                    print("")
                    check3 = True

                if (check1 and check2 and check3):
                    ################################################################

                    ## check which measurement type you want to upload  in your profile
                    measurement_type = str(values["SMP-keywords"])

                    # extract dir from fullpath
                    dironly = path_head(fullpath_json_main)

                    # extract filename from fullpath
                    filename_ext = path_tail(fullpath_json_main)
                    filename = os.path.splitext(filename_ext)[0]

                    # define output path + file name + measurement type
                    d_out = os.path.join(dironly, filename + "_NIVIZ_" + measurement_type + ".json")

                    # extract output file name
                    filename_new = path_tail(d_out)

                    ################################################################

                    # load the input file from disk
                    print('')
                    print(f"[INFO] Loading {measurement_type} -Profile from {filename_ext}...")
                    print('')

                    smp = pd.read_csv(values["-IN-smp-derivatives"], sep=',')

                    # extract smp_depth (mm)
                    smp_depth = smp.iloc[:,0]

                    #convert to cm
                    smp_depth_cm = smp_depth/10

                    # extract smp_N values (N)
                    smp_1 = smp.iloc[:,1]
                    # extract smp_rho values (kg/m3)
                    smp_2 = smp.iloc[:,6]
                    # extract SSA values (m2/kg)
                    smp_3 = smp.iloc[:,7]
                    # convert to string

                    # info for the selected measurement type
                    if measurement_type == "hardness":
                        smp = smp_1
                        unit = "N"
                        measurement = "smp"
                        comment = "SMP C&R2020 Hardness"
                    if measurement_type == "density":
                        smp = smp_2
                        unit = "kg/m3"
                        measurement = "density"
                        comment = "SMP C&R2020 Density"
                    if measurement_type == "ssa":
                        smp = smp_3
                        unit = "m2/kg"
                        measurement = "ssa"
                        comment = "SMP C&R2020 SSA"

                    # how many measurements are averaged together (e.g. every 1cm, raw steps of smp 0.05cm --> smp_steps=20 )
                    smp_steps = 60  # 1step is 0.05 mm so that with 600 every 3cm there is an average measurment like for example with the snowcutter

                    ################################################################

                    # smp to json
                    smp_profile = smp_to_json(smp_depth_cm, smp, unit, measurement, comment, smp_steps)

                    ################################################################

                    ### merge smp json into exitsing json file
                    merged_json = migrate_sub_part_into_json(smp_profile, measurement, fullpath_json_main)

                    ## update timestamp in newly merged json
                    merged_json = update_timestamp(merged_json)

                    print(merged_json)

                    # dump merged_json and save to disk
                    with open(d_out, "w") as json_merge:
                        merged_json = json.loads(merged_json)
                        json.dump(merged_json, json_merge, default = myconverter)

                     # done, give feedack to user
                    print('[INFO] Done! :-)')
                    print('')
                    print(f'[INFO] Added smp "{measurement_type}" file to "{filename_json_main}"')
                    print('')

                    #reset checkmarks
                    check1 = False
                    check2 = False
                    check3 = False
                    ################################################################

        except Exception as e:
            tb = traceback.format_exc()
            sg.Print(f'Ups, an error occured =( \n\n Here is the info:', e, tb)
            window.FindElement('-OUTPUT-').Update('')

################################################################
###############   Plotting SMP  ################################
################################################################

    if event == 'Create SMP Plot':
        try:
                ### clear printed output
                window.FindElement('-OUTPUT-').Update('')

             # function to fix path OS vulnerability
                def path_tail(path):
                    head, tail = ntpath.split(path)
                    return tail or ntpath.basename(head)

                def path_head(path):
                    head, tail = ntpath.split(path)
                    return head or ntpath.basename(tail)

                # full path (dir + file + ext)
                path = values['-IN-smp-derivatives']
                directory = str(path_head(path))

                # create csv with filenames (only _samples.csv)
                print('')
                print(f"[INFO] Accessing SMP files...")
                print('')

                with open(directory+"/filenames.csv", 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow(["filenames"])
                    for entry in os.scandir(directory):
                        if entry.is_file():
    #                        if entry.name[-9:] == "mples.csv":
                            if entry.name[-8:] == "meta.csv":
                                writer.writerow([entry.name])
                                print(f"{entry.name}...")
                            else:
                                pass
                    f.close()

                # get smp derivative files

                globpath = str(f'{directory}\*derivatives.csv' )

                # smp_data_files = sorted(glob.glob(globpath))

                csvpath = str(directory+"/filenames.csv")
                smp_df = pd.read_csv(csvpath)

                # plot
                print('')
                print('[INFO] Plotting figure...')
                print('')

                # define startnumber
                if smp_df.filenames.iloc[0][0:3] == 'lar':
    #                start_number = int(smp_df['filenames'].iloc[0][16:-12])
                    start_number = int(smp_df['filenames'].iloc[0][16:-9])
                elif smp_df.filenames.iloc[0][0:3] == 'LAR':
                    start_number = int(smp_df['filenames'].iloc[0][5:8])
                elif smp_df.filenames.iloc[0][0:3] == 'ABO':
                    start_number = int(smp_df['filenames'].iloc[0][5:8])
                elif smp_df.filenames.iloc[0][0:3] == 'S52':  #'S36':':
                    start_number = int(smp_df['filenames'].iloc[0][5:8])
                else:
                    print("check if the files in the directory follow the naming convention: either laret-yyyymmdd-###_derivatives.csv OR ABOA####_derivatives.csv")

                start = 1
                len_smp_df = len(smp_df.index) +1
                len_list = np.arange(1,len_smp_df)

                #create 3 different plots with different median force scales
                #three different force sclaes
                contour_levels_2 = np.arange( 0, 10, 0.025)   # 400 counts
                contour_levels_3 = np.arange( 0, 0.2, 0.0005) # 400 counts
                d=1

                for d in range(1,6):
                    for index,row in smp_df.iterrows():
                            # hardcoded: location of SMP # characters in string (depending on naming convention... laret-yyyymmdd_nnn.csv or ABOAnnnn.csv)
                            if row['filenames'][0:3] == 'lar':
                            #   fn = directory  +"/" + row['filenames'][0:-11] + "derivatives.csv"
                                fn = directory  +"/" + row['filenames'][0:-8] + "derivatives.csv"
                            elif row['filenames'][0:3] == 'LAR':
                                fn = directory  +"/" + row['filenames'][0:9] + "derivatives.csv"
                            elif row['filenames'][0:3] == 'ABO':
                                fn = directory  +"/" + row['filenames'][0:9] + "derivatives.csv"
                            elif row['filenames'][0:3] == 'S36':
                                fn = directory  +"/" + row['filenames'][0:9] + "derivatives.csv"
                            elif row['filenames'][0:3] == 'S52':
                                fn = directory  +"/" + row['filenames'][0:9] + "derivatives.csv"
                            else:
                                print("check if the files in the directory follow the naming convention: either laret-yyyymmdd_###_derivatives.csv OR ABOA####_derivatives.csv")

                            df = pd.read_csv(fn, header=0)
                            Y = df['force_median [N]']
                            z = df['distance [mm]']

                            i = index
                            i0 = start

                            x1 = i0 + i * 1
                            x2 = i0 + (i+1) * 1

                            if d == 1:
                                #norm= matplotlib.colors.LogNorm(vmin=np.array([Y,Y]).transpose().min(), vmax=np.array([Y,Y]).transpose().max())
                                #plt.contourf([x1, x2], z, np.array([Y,Y]).transpose(), locator=ticker.LogLocator(base=1.8,  subs=range(1,10)), norm=norm, cmap='jet') #log scale for continues color bar
                                plt.contourf([x1, x2], z, np.array([Y,Y]).transpose(), locator=ticker.LogLocator(base=1.8), cmap='jet') #log scale
                            if d == 2:
                                plt.contourf([x1, x2], z, np.array([Y,Y]).transpose(), levels=contour_levels_2, cmap='jet')
                            if d == 3:
                                plt.contourf([x1, x2], z, np.array([Y,Y]).transpose(), levels=contour_levels_3, cmap='jet')
                            if d == 4:
                                YY = df['CR2020_density [kg/m^3]']
                                contour_levels_4 = np.arange( 0, 400, 1) # 400 counts
                                plt.contourf([x1, x2], z, np.array([YY,YY]).transpose(), levels=contour_levels_4, cmap='jet')
                            if d == 5:
                                YYY = df['CR2020_ssa [m^2/kg]']
                                contour_levels_5 = np.arange( 0, 70, 0.25)
                                plt.contourf([x1, x2], z, np.array([YYY,YYY]).transpose(), levels=contour_levels_5, cmap='jet')


                    plt.xlabel('SMP (#)')
                    plt.ylabel('Depth (mm)')
                    plt.gca().invert_yaxis()
                    plt.tight_layout()
                    cbar = plt.colorbar()
                    plt.grid()

                    if d == 1:
                        imagefilename = str(directory)+"/plot_log_scale"
                        cbar.set_label('Median force (N)', rotation=90)
                    if d == 2:
                        imagefilename = str(directory)+"/plot_higher_forces"
                        cbar.set_label('Median force (N)', rotation=90)
                    if d == 3:
                        imagefilename = str(directory)+"/plot_lower_forces"
                        cbar.set_label('Median force (N)', rotation=90)
                    if d == 4:
                        imagefilename = str(directory)+"/plot_smp_density"
                        cbar.set_label('Calonne and Richter 2020 Density (kg/m3)', rotation=90)
                    if d == 5:
                        imagefilename = str(directory)+"/plot_smp_ssa"
                        cbar.set_label('Calonne and Richter 2020 SSA (m2/kg)', rotation=90)

                    plt.savefig(imagefilename,dpi=300)
                    plt.clf()

                # hardcoded: location of SMP # characters in string (depending on naming convention... laret-yyyymmdd_nnn.csv or ABOAnnnn.csv)
                if row['filenames'][0:3] == 'lar':
                    plt.xticks(len_list, labels=smp_df.filenames.str[15:-12], rotation=-45, ha="left")
                elif row['filenames'][0:3] == 'LAR':
                    plt.xticks(len_list, labels=smp_df.filenames.str[4:8], rotation=-45, ha="left")
                elif row['filenames'][0:3] == 'ABO':
                    plt.xticks(len_list, labels=smp_df.filenames.str[4:8], rotation=-45, ha="left")
                elif row['filenames'][0:3] == 'S52':
                    plt.xticks(len_list, labels=smp_df.filenames.str[4:8], rotation=-45, ha="left")
                else:
                    print("check if the files in the directory follow the naming convention: either laret-yyyymmdd_###_derivatives.csv OR ABOA####_derivatives.csv")


                print('[INFO] Done! :-)')
                print('')
                print(f'[INFO] Five Figures saved: "{imagefilename}"')
                print('')
        # Error handling
        except Exception as ex:
             tb2 = traceback.format_exc()
             sg.Print(f'Ups, an error occured =( "check if the files in the directory follow the naming convention: either laret-yyyymmdd_###_derivatives.csv OR ABOA####_derivatives.csv" \n\n Further please check if you used the Calonne and Richter 2020 parametrization in the pyngui export.\n\nHere is more info:', ex, tb2)
             #window.FindElement('-OUTPUT-').Update('')

################################################################
##########          Make a Json from table    ##################
################################################################

    if event == 'Table to Json':
        try:
                ### clear printed output
                window.FindElement('-OUTPUT-').Update('')

                ###############################################################################
                ### Function: Make json out of table values
                ###############################################################################
                def make_json_from_table(profile_type):
                    ## to evalueate if user wants to create a point profile or not
                    comment = values['text_comment']
                    method = values['table_keyword']
                    table_profile = ""
                    ### check if tehre are entries in the table
                    if values[(0,0)] != "":
                        # differntiate because it is not a pointprofile
                        if profile_type == "Temperature":
                            json_values = ""
                            head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "true" + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                            tail = "]}"
                            i = 0
                            ### check if the certain cell values['i,j'] is filled with a number and if true, continue
                            while values[(i,0)] != "":
                                # condition to check weather all cells are filled which are neccessary
                                if (values[(i,0)] != "" and values[(i,2)] != ""):
                                    json_values = json_values + "{" + chr(34) + "value" + chr(34) + ":{" + chr(34) + "depth" + chr(34) + ":" + str(values[(i,0)]) + "," + chr(34) + "value" + chr(34) + ":" + str(values[(i,2)]) + "}" + "},"
                                else:
                                    print("Not all values were entered: Please check if you have entered at every row a depth and a temperature value ;)")
                                i = i + 1
                        # differntiate because it is not a pointprofile
                        else:
                            i = 0
                            json_values = ""
                            while values[(i,0)] != "":
                                head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "false" + chr(34) + ", "  + chr(34) +  "method" + chr(34) + ":" + chr(34) + method + chr(34) + ", "  + chr(34) +  "comment" + chr(34) + ":" + chr(34) + comment + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                                tail = "]}"
                                # condition to check weather all cells are filled which are neccessary
                                if (values[(i,0)] != "" and values[(i,1)] != "" and  values[(i,2)] != ""):
                                    json_values = json_values + "{" + chr(34) + "top" + chr(34) + ":" + str(values[(i,0)]) +  "," + chr(34) + "bottom" + chr(34) + ":" + str(values[(i,1)]) + "," + chr(34) + "value" + chr(34) + ":" + str(values[(i,2)]) + "},"
                                else:
                                    print("Not all values were entered. Please check if you have entered at every row a top, bottom and value ;)")
                                i = i + 1
                        json_values = str(json_values[0:(len(json_values)-1)])
                        table_profile = head + json_values + tail
                    else:
                         print("No values were entered. Please check if you have entered values at the top of the table ;)")

                    return table_profile

                ### what kind of profile do you wqnt
                profile_type = values['table_keyword']

                ### generate a path and name to the proifle
                profile_name =  str(profile_type) + "_table_profil"

                ### path where to safe it
                profile_path = values['-Out-table-json-']

                ### check if some input is missing
                if (profile_path == ""):
                    print("The path to save the created profile was not defined. Please enter a valid path ;) ")
                elif (profile_type == ""):
                    print("No profile type entered. Please enter which type you want insert into the new profile ;)")
                else:
                    # define output path + file name + measurement type
                    d_out = os.path.join(profile_path, profile_name + ".json")

                    ### make json out of the values in the Table
                    json_profile = str(make_json_from_table(profile_type))

                    # dump json_profile and save to disk
                    ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
                    with open(d_out, "w") as profile_json:
                        json_profile = json.loads(json_profile)
                        json.dump(json_profile, profile_json, default = myconverter)

                    # give feedback
                    print('[INFO] Done! :-)')
                    print('')
                    print(f'[INFO] new .json saved: "{filename_json_main}"')
                    print('')
                    ################################################################

        except Exception as ex:
             tb2 = traceback.format_exc()
             sg.Print(f'Ups, an error occured =( :', ex, tb2)
             #window.FindElement('-OUTPUT-').Update('')

################################################################
##########   Add values from table to existing Profile #########
################################################################

    if event == 'Add values to Profile':
        try:
                ### clear printed output
                window.FindElement('-OUTPUT-').Update('')

                ###############################################################################
                ### Function: Make json out of table values
                ###############################################################################
                def make_json_from_table(profile_type):
                    ## to evalueate if user wants to create a point profile or not
                    comment = values['text_comment']
                    method = values['table_keyword']
                    table_profile = ""
                    ### check if tehre are entries in the table
                    if values[(0,0)] != "":
                        # differntiate because it is not a pointprofile
                        if profile_type == "Temperature":
                            json_values = ""
                            head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "true" + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                            tail = "]}"
                            i = 0
                            ### check if the certain cell values['i,j'] is filled with a number and if true, continue
                            while values[(i,0)] != "":
                                # condition to check weather all cells are filled which are neccessary
                                if (values[(i,0)] != "" and values[(i,2)] != ""):
                                    json_values = json_values + "{" + chr(34) + "value" + chr(34) + ":{" + chr(34) + "depth" + chr(34) + ":" + str(values[(i,0)]) + "," + chr(34) + "value" + chr(34) + ":" + str(values[(i,2)]) + "}" + "},"
                                else:
                                    print("Not all values were entered: Please check if you have entered at every row a depth and a temperature value ;)")
                                i = i + 1
                        # differntiate because it is not a pointprofile
                        else:
                            i = 0
                            json_values = ""
                            while values[(i,0)] != "":
                                head = "{" + chr(34) + "meta" + chr(34) + ":{" + chr(34) + "pointprofile" + chr(34) + ":" + chr(34)+ "false" + chr(34) + ", "  + chr(34) +  "method" + chr(34) + ":" + chr(34) + method + chr(34) + ", "  + chr(34) +  "comment" + chr(34) + ":"  + chr(34) + comment + chr(34) + "}," + chr(34) + "layers" + chr(34) + ":["
                                tail = "]}"
                                # condition to check weather all cells are filled which are neccessary
                                if (values[(i,0)] != "" and values[(i,1)] != "" and  values[(i,2)] != ""):
                                    json_values = json_values + "{" + chr(34) + "top" + chr(34) + ":" + str(values[(i,0)]) +  "," + chr(34) + "bottom" + chr(34) + ":" + str(values[(i,1)]) + "," + chr(34) + "value" + chr(34) + ":" + str(values[(i,2)]) + "},"
                                else:
                                    print("Not all values were entered. Please check if you have entered at every row a top, bottom and value ;)")
                                i = i + 1
                        json_values = str(json_values[0:(len(json_values)-1)])
                        table_profile = head + json_values + tail
                    else:
                         print("No values were entered. Please check if you have entered values at the top of the table ;)")

                    return table_profile

                ### what kind of profile do you wqnt
                profile_type = values['table_keyword']

                ### generate a path and name to the proifle
                profile_name =  str(profile_type) + "_table_profil"

                ## path to main json
                fullpath_json_main = values['-IN-main-json-4']

                ### check if some input is missing
                if (fullpath_json_main == ""):
                    print("No main json file entered. Please browse after a main json file you want insert into the new profile ;)")
                elif (profile_type == ""):
                    print("No profile type entered. Please enter which type you want insert into the new profile ;)")
                else:
                    filename_json_main = path_tail(fullpath_json_main)
                    fullpath_json_folder = path_head(fullpath_json_main)

                    # define output path + file name + measurement type
                    d_out = os.path.join(fullpath_json_folder, filename_json_main)

                    ### make json out of the values in the Table
                    json_profile = str(make_json_from_table(profile_type))
                    json_profile = str(migrate_sub_part_into_json(json_profile, profile_type, fullpath_json_main))

                    # dump json_profile and save to disk
                    ### info: default=myconverter is used to serialize datetime object (function "myconverter" defined above)
                    with open(d_out, "w") as profile_json:
                        json_profile = json.loads(json_profile)
                        json.dump(json_profile, profile_json, default = myconverter)

                    print('[INFO] Done! :-)')
                    print('')
                    print(f'[INFO] new .json saved: "{filename_json_main}"')
                    print('')
                    ################################################################

        except Exception as ex:
             tb2 = traceback.format_exc()
             sg.Print(f'Ups, an error occured =( :', ex, tb2)
             #window.FindElement('-OUTPUT-').Update('')

################################################################
##########   Add values from table to existing Profile #########
################################################################

    if event == 'Clear Table':
        try:
                ### clear input table
                for key in values:
                    ### clear cells
                    for i in range(MAX_ROWS):
                        for j in range(MAX_COL):
                            window[(i, j)].update('')





                print('[INFO] Done! :-)')
                print('')
                print(f'[INFO] Table cleared')
                print('')
                ################################################################

        except Exception as ex:
             tb2 = traceback.format_exc()
             sg.Print(f'Ups, an error occured =( :', ex, tb2)
             #window.FindElement('-OUTPUT-').Update('')
