/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  angular
    .module('niviz.menu', [
      'niviz.lib',
      'niviz.settings',
      'niviz.console'
    ])

    .controller('MenuController', [
      '$scope',
      '$console',
      'niviz',
      'settings',
      'create',
      'modal-local-files',

      function (scope, console, niviz, settings, create, modalLocalFiles) {
        scope.collapsed = true;

        scope.settings = function () {
          settings([
            niviz.Common.defaults,
            niviz.SLFProfile.defaults,
            niviz.SimpleProfile.defaults,
            niviz.Meteograph.defaults,
            niviz.ProParser.codes,
            niviz.Timeline.defaults,
            niviz.CAAML.defaults
          ])
          .then(function (modules) {
            scope.$root.$broadcast('settings');
            return modules.forEach(function (c) {
              c.store();
            });
          });
        };

        scope.create = function () {
          create(scope.profiles).then(function () {});
        };

        scope.openLocalStorage = function () {
          modalLocalFiles().then(function () {});
        };

      }
    ]);

}(angular, jQuery));
