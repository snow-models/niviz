/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $, moment, niviz) {
  'use strict';

  var t = niviz.Translate.gettext;

  // Function to create and show the modal
  function showYRangeModal(options, callback) {
    // Create the modal shadow div
    var modal_shadow = document.createElement('div');
    modal_shadow.style.position = 'fixed';
    modal_shadow.style.top = '0';
    modal_shadow.style.left = '0';
    modal_shadow.style.right = '0';
    modal_shadow.style.bottom = '0';
    modal_shadow.style.backgroundColor = '#000';
    modal_shadow.style.opacity = '0.5';
    modal_shadow.style.zIndex = '1040';

    // Create the modal div
    var modal = document.createElement('div');
    modal.style.position = 'fixed';
    modal.style.top = '50%';
    modal.style.left = '50%';
    modal.style.transform = 'translate(-50%, -50%)';
    modal.style.border = '1px solid rgba(0,0,0,.2)';
    modal.style.borderRadius = '6px';
    modal.style.padding = '20px';
    modal.style.backgroundColor = '#fff';
    modal.style.zIndex = '1050';
    modal.style.boxShadow = '0 5px 15px rgba(0,0,0,.5)';
    // modal.style.minWidth = '200px';
    modal.style.whiteSpace = 'nowrap';

    var title = document.createElement('b');
    title.style.display = 'block';
    title.style.marginBottom = '0.75em';
    title.textContent = t('adjust_y_axis_range', 'ui');
    modal.appendChild(title);

    // Create the select element with options taken from the argument
    var select = document.createElement('select');
    select.title = t('tl_params_param', 'ui');
    select.style.marginRight = '0.5em';
    options.forEach(function(option) {
      // Use option both as text and value
      select.options.add(new Option(option, option));
    });
    modal.appendChild(select);

    // Create the y_value_bottom numeric input
    var yValueBottom = document.createElement('input');
    yValueBottom.type = 'number';
    yValueBottom.style.marginRight = '0.5em';
    yValueBottom.style.width = '8ch';
    yValueBottom.placeholder = t('tl_params_min', 'ui');
    modal.appendChild(yValueBottom);

    // Create the y_value_top numeric input
    var yValueTop = document.createElement('input');
    yValueTop.type = 'number';
    // yValueTop.style.marginRight = '0.5em';
    yValueTop.style.width = '8ch';
    yValueTop.placeholder = t('tl_params_max', 'ui'); // Set placeholder text
    modal.appendChild(yValueTop);

    // break line
    modal.appendChild(document.createElement('br'));

    // Create the confirm button
    var confirmBtn = document.createElement('button');
    confirmBtn.textContent = t('Apply', 'ui');
    confirmBtn.style.float = 'right';
    confirmBtn.style.marginTop = '0.75em';
    confirmBtn.style.marginLeft = '0.5em';
    confirmBtn.onclick = function() {
      var data = {
        selectValue: select.value,
        yValueBottom: yValueBottom.value, // Use the new input names
        yValueTop: yValueTop.value
      };
      callback(data); // Call the callback function with the input values
      document.body.removeChild(modal_shadow);
      document.body.removeChild(modal);
    };
    modal.appendChild(confirmBtn);

    // Create the cancel button
    var cancelBtn = document.createElement('button');
    cancelBtn.textContent = t('Cancel', 'ui');
    cancelBtn.style.float = 'right';
    cancelBtn.style.marginTop = '0.75em';
    cancelBtn.style.marginLeft = '0.5em';
    cancelBtn.onclick = function() {
      document.body.removeChild(modal_shadow);
      document.body.removeChild(modal);
    };
    modal.appendChild(cancelBtn);

    // Append the modal to the body
    document.body.appendChild(modal_shadow);
    document.body.appendChild(modal);
  }


  angular
    .module('niviz.meteographer', [
      'niviz.lib',
      'niviz.util'
    ])

    .controller('MeteoGrapherController', [
      '$scope',

      function($scope) {
        $scope.dropdownSettings = {
          scrollableHeight: '250px',
          scrollable: true,
          showCheckAll: false
        };

        $scope.example5customTexts = {
          buttonDefaultText: t('_dr_buttonDefaultText', 'ui'),
          dynamicButtonTextSuffix: t('_dr_dynamicButtonTextSuffix', 'ui'),
          checkAll: t('_dr_checkAll', 'ui'),
          uncheckAll: t('_dr_uncheckAll', 'ui')
        };

        $scope.dropdownEvents = {
          onSelectionChanged: function(item) {
            var value = $scope.set();
          }
        };
      }
    ])

    .directive('meteographer', [
      'niviz',
      'print',
      '$timeout',
      '$location',
      'registry',

      function (niviz, print, timeout, location, registry) {
        return {
          restrict: 'A',
          templateUrl: 'meteographer.html',
          scope: { meteo: '=', close: '&onClose', width: '=', cart: '=', lang: '=', hscale: '=' },
          link: function (scope, element, attrs) {

            var W = $(window),
                height = element.parent().height() || 420,
                width  = W.width(), maxwidth = scope.width || 900,
                canvas = $('#canvas', element), elem = $(element),
                date = moment(location.search().date, 'YYYYMMDDTHHmm'),
                hashcode = new Date().getTime();

            element.height(Math.min(height, 420));

            scope.parameters = [];
            scope.selected = [];
            scope.autoscale = false;
            scope.use_settings = true;

            scope.print = function () {
              print(canvas, scope.meteo && scope.meteo.filename || null);
            };

            scope.save = function (type) {
              var name = scope.graph.data.name + '.meteo';
              scope.$parent.save(type, scope.graph, name);
            };

            function update () {
              if (!scope.meteo) return;

              if (scope.cart) {

                height = element.parent().height() - 10;
                elem.width(scope.width);
                elem.height(height / (scope.hscale || 1) - (scope.hscale || 1 - 1) * 5);
                canvas.width(elem.width() - 94);

                if (scope.selected && scope.selected.length > 1) {
                  canvas.width(elem.width() - 80);
                }

                canvas.css('margin-left', '52px');

              } else {
                //console.dir(element.parent().height());
                // height = Math.min(element.parent().height() - 60, 420);
                // elem.width(Math.min(Math.max(width, maxwidth - 20), maxwidth - 20));
                // elem.height(height + 50);
                height = Math.max(element.height(), 100);
                width = Math.max(element.width(), maxwidth)
                element.height(height)
                element.width(width)
                canvas.width(elem.width() - 20);
              }

              canvas.height(Math.max(elem.height() - 50, 100));

              if (!scope.graph) {
                scope.graph = niviz.draw('Meteograph', scope.meteo, canvas, {});
                if (date.isValid()) scope.graph.current(date);
                registry.add(hashcode, scope.graph);
              } else {
                scope.graph.draw();
              }

              scope.initParameters();

              var select = $('.dropdownSelect', elem);
              select.width(300);
              select.offset({ left: scope.graph.properties.br.x - 288 + elem.offset().left});
              //scope.set();
            }

            scope.initParameters = function () {
              scope.parameters = scope.graph.all.map(function (p, i) {
                return { id: i, label: p };
              });

              scope.selected = scope.parameters.filter(function (p) {
                if (scope.graph.parameters.indexOf(p.label) > -1) return true;
                return false;
              });
            };

            scope.set = function (value) {
              if (scope.graph) {
                var array = scope.selected.map(function (e) {
                  var i = 0;
                  for (i = 0; i < scope.parameters.length; ++i) {
                    if (scope.parameters[i].id === e.id) break;
                  }
                  return scope.parameters[i] && scope.parameters[i].label;
                });

                scope.graph.set(array);
                update();
              }
            };

            scope.toggleAutoscale = function () {
              scope.autoscale = !scope.autoscale;
              scope.graph.setProperties({ autoscale: scope.autoscale });
              scope.set();
            };

            scope.changeValueRange = function () {
              showYRangeModal(scope.graph.curves.map(function(c) { return c.name; }), function (choices) {
                scope.autoscale = false;
                scope.graph.set_y_range(choices.selectValue, choices.yValueBottom, choices.yValueTop);
                scope.set();
              });
            };

            scope.setPriority = function (priority) {
              scope.use_settings = (priority === 'settings');
              scope.graph.setProperties({ use_settings: scope.use_settings });
              scope.set();
            };


            var oldwidth = 0, oldheight = 0, tListener = null;
            var observer = new MutationObserver(function(mutations) {
              var h = element.height(), w = element.width();

              if (h !== oldheight || w !== oldwidth) {
                clearTimeout(tListener)
                tListener = setTimeout(function () {
                  update();
                }, 500);
              }

            });

            observer.observe(element[0], { attributes: true });

            // The following lines are inspired from:
            // ui/javascripts/timeliner.js lines 174-188
            var __arrow_key_handler = function (e) {
              if (e.keyCode === 39 || e.keyCode === 37) {
                // Disable highlight with mouse
                scope.graph.is_keyboard_highlighting = true;
                // Clear pending timeout to reset to mouse highlight
                if (scope.graph.last_keyboard_highlighting_timeout) {
                  clearTimeout(scope.graph.last_keyboard_highlighting_timeout);
                }
                // Reset to mouse highlight after some time
                scope.graph.last_keyboard_highlighting_timeout = setTimeout(function(){
                  scope.graph.is_keyboard_highlighting = false;
                }, 3000);
                // Change highlight
                if (e.keyCode === 39) { // right arrow key
                  scope.graph.highlight(scope.graph.last_highlighted_index + 1);
                } else if (e.keyCode === 37) { // left arrow key
                  scope.graph.highlight(scope.graph.last_highlighted_index - 1);
                }
              }
            };
            var doc = $(document);
            doc.on('keydown', __arrow_key_handler);

            scope.$on('$destroy', function () {
              doc.off('keydown', __arrow_key_handler);
              observer.disconnect();
              clearTimeout(tListener);
              registry.remove(hashcode);
            });

            scope.$watch('width', function (w) {
              // console.dir(w)
              maxwidth = scope.width || 900;
              update();
            });

            scope.$watch('$parent.height + hscale', function (h) {
              // console.log(h)
              update();
            });

            scope.$on('settings', function (e, c) {
              scope.set();
            });

            scope.$watch('meteo', function (meteo) {
              if (meteo) {
                scope.meteo = meteo;
                update();
              }
            });
          }
        };
      }
    ]);

}(angular, jQuery, moment, niviz));
