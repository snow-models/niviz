/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  function checkstb (scope) {
    var text = '';

    scope.profile.stability.forEach(function (s) {
      if (s.type !== 'flags') {
        if (text) text += ', ';
        text += s.symbol;
      }
    });

    return text;
  }

  function checkmeta (scope) {
    var text = '', meta = scope.station.meta, pm = scope.profile.meta;

    if (meta.position.altitude && meta.position.longitude && meta.position.latitude)
      text += 'Location'

    if (pm.sky || pm.ta || (pm.wind && (pm.wind.speed || pm.wind.dir))) {
      if (text) text += ', ';
      text += 'weather'
    }

    return text;
  }

  angular
    .module('niviz.create', [
      'niviz.util',
      'niviz.lib'
    ]).

  // Create new profile modal
  factory('create', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (modules) {

        return modal
          .open({
            animation: true,
            templateUrl: 'create.html',
            controller: 'CreateController',
            windowClass: 'modal-creator',
            backdrop: 'static',
            resolve: {
              modules: function () { return modules; }
            }
          })
          .result
          .catch(function (reason) {
            return q.reject(reason);
          });
      };
    }
  ]).

  controller('CreateController', [
    '$scope',
    '$uibModalInstance',
    'modules',
    'save',
    'niviz',
    //'modal-two-buttons',

    function (scope, modal, modules, save, niviz/*, mtb*/) {
      scope.control = {};

      scope.station = new niviz.Station();
      scope.profile = new niviz.Profile(moment());
      scope.station.name = 'StationName';
      scope.station.push(scope.profile);

      scope.save = function () {
        if (scope.control.savemeta) scope.control.savemeta();

        modules.push(scope.station);
        modal.close(modules);
      };

      scope.modules = [
        { id: 1, name: 'initial' },
        { id: 2, name: 'main' },
        { id: 3, name: 'meta' },
        { id: 4, name: 'startigraphy' }
      ];

      scope.active = 1;

      scope.checkstb = function () {
        return checkstb(scope);
      };

      scope.checkmeta = function () {
        return checkmeta(scope);
      };

      scope.goto = function (i) {
        scope.active = i;
      };

      scope.cancel = function () {
        // mtb().then(function (d) {
        // });
        var r = confirm("Are you sure? All unsaved data will be lost");
        if (r == true) modal.dismiss('cancel');
      };
    }
  ]).

  factory('editprofile', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (modules) {

        return modal
          .open({
            animation: true,
            templateUrl: 'create.html',
            controller: 'EditController',
            windowClass: 'modal-creator',
            backdrop: 'static',
            resolve: {
              modules: function () { return modules; }
            }
          })
          .result
          .catch(function (reason) {
            return q.reject(reason);
          });
      };
    }
  ]).

  controller('EditController', [
    '$scope',
    '$uibModalInstance',
    '$timeout',
    'modules',
    'save',
    'niviz',

    function (scope, modal, $timeout, modules, save, niviz) {
      scope.control = {};
      scope.header = '_editprofile';

      scope.station = modules.station;
      scope.profile = modules.profile;

      scope.repaint = function () {
        $timeout(function () {
          scope.station.emitter.emit('profile', scope.profile, true);
        });
      };

      scope.save = function () {
        // if (scope.control.savemeta) scope.control.savemeta();
        modal.close(modules);
      };

      scope.modules = [
        { id: 1, name: 'initial' },
        { id: 2, name: 'main' },
        { id: 3, name: 'meta' },
        { id: 4, name: 'startigraphy' }
      ];

      scope.active = 1;

      scope.checkstb = function () {
        return checkstb(scope);
      };

      scope.checkmeta = function () {
        return checkmeta(scope);
      };

      scope.goto = function (i) {
        scope.active = i;
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]);

}(angular, jQuery));
