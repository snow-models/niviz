/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  angular
    .module('niviz.settings', [
      'ui.bootstrap.tabs',
      'ui.select',
      'niviz.util',
      'niviz.lib'
    ]).

  // Modal to access the locally stored profiles
  factory('modal-local-files', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (properties) {

        return modal
          .open({
            animation: true,
            templateUrl: 'modal-local-files.html',
            controller: 'ModalLocalFilesController',
            windowClass: 'modal-localStorage',
            backdrop: 'static',
            resolve: {
              data: function () { return properties; }
            }
          })
          .result
          .catch(function (/* reason */) {
            // cancel btn pressed
          });
      };
    }
  ]).

  controller('ModalLocalFilesController', [
    '$scope',
    '$uibModalInstance',
    'data',
    '$rootScope',

    function (scope, modal, graph, $rootScope) {

      var t = niviz.Translate.gettext;

      scope.files = JSON.parse(localStorage.getItem('fileStore')) || {};
      scope.keys = Object.keys(scope.files);
      scope.dates = {};

      scope.keys.forEach(function (key) {
        scope.dates[key] = moment(scope.files[key].lastedited);
      });

      scope.keys.sort(function (a, b) {
        return scope.dates[b].diff(scope.dates[a]);
      });

      scope.open = function (key) {
        $rootScope.$broadcast('jsonfile', scope.files[key]);
        modal.dismiss();
      };

      scope.rm = function (key) {
        var r = confirm(t('_local_rm_profile', 'ui'));
        if (r === false) return;
        delete scope.files[key];
        delete scope.dates[key];

        scope.keys.splice(scope.keys.indexOf(key), 1);

        localStorage.setItem('fileStore', JSON.stringify(scope.files));
      };

      scope.rmAll = function () {
        var r = confirm(t('_local_rm_profiles', 'ui'));
        if (r === false) return;
        scope.files = {};
        scope.dates = {};
        scope.keys = [];

        localStorage.setItem('fileStore', JSON.stringify(scope.files));
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]).

  // The modal for extracting a time series from a timeline graph
  factory('extract-time-series', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (properties) {

        return modal
          .open({
            animation: true,
            templateUrl: 'timeline-modal-extract.html',
            controller: 'TimelineETSController',
            resolve: {
              data: function () { return properties; }
            }
          })
          .result
          .catch(function (/* reason */) {
            // cancel btn pressed
          });
      };
    }
  ]).

  controller('TimelineETSController', [
    '$scope',
    '$uibModalInstance',
    'data',

    function (scope, modal, graph) {
      scope.parameters = graph.parameters;
      scope.data = {
        height: null,
        parameter: scope.parameters[0],
        isdepth: '1',
        functor: 'VALUE'
      }

      scope.save = function () {
        scope.data.isdepth = !!parseInt(scope.data.isdepth)
        modal.close(scope.data);
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]).

  // The modal for save as... dialog
  factory('save-as-modal', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (properties) {

        return modal
          .open({
            animation: true,
            templateUrl: 'modal-save-as.html',
            controller: 'SaveAsController',
            resolve: {
              data: function () { return properties; }
            }
          })
          .result
          .catch(function (/* reason */) {
            // cancel btn pressed
          });
      };
    }
  ]).

  controller('SaveAsController', [
    '$scope',
    '$uibModalInstance',
    'data',

    function (scope, modal, data) {
      scope.filename = data;

      scope.save = function () {
        modal.close(scope.filename);
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]).

  // The modal for setting the boundaries of the snow height axis
  // in the timeline graph
  factory('timeline-hs-settings', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (properties) {

        return modal
          .open({
            animation: true,
            templateUrl: 'timeline-modal-height.html',
            controller: 'TimelineHSController',
            resolve: {
              data: function () { return properties; }
            }
          })
          .result
          .catch(function (/* reason */) {
            // cancel btn pressed
          });
      };
    }
  ]).

  controller('TimelineHSController', [
    '$scope',
    '$uibModalInstance',
    'data',

    function (scope, modal, data) {
      scope.minvalue = data.hsmin !== undefined ? data.hsmin : '';
      scope.maxvalue = data.hsmax !== undefined ? data.hsmax : '';

      scope.save = function () {
        modal.close({ min: scope.minvalue, max: scope.maxvalue });
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]).

  // Settings modal
  factory('settings', [
    '$uibModal',
    '$q',
    'zip',

    function (modal, q, zip) {
      return function (modules) {

        function restore(m) { m[0].merge(m[1]); }
        function values(c)  { return c.values; }

        var backups = modules.map(values);

        return modal
          .open({
            animation: true,
            templateUrl: 'settings.html',
            controller: 'SettingsController',
            resolve: {
              modules: function () { return modules; }
            }
          })
          .result
          .catch(function (reason) {
            zip(modules, backups).forEach(restore);
            return q.reject(reason);
          });
      };
    }
  ]).

  controller('SettingsController', [
    '$scope',
    '$uibModalInstance',
    'modules',
    'save',

    function (scope, modal, modules, save) {
      var input = $('<input type="file"/>').on('change', open);

      scope.modules = modules;

      scope.save = function () {
        modal.close(modules);
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };

      scope.import = function () {
        input[0].click();
      };

      scope.export = function () {
        save('settings.json', 'application/json', JSON.stringify(config()));
      };

      scope.reset = function () {
        window.localStorage.clear();
        window.location.reload();
      };

      scope.tabdeselect = function (name) {
        if (name === 'Pro Parser') {
          niviz.ProParser.getcodes();
        }
      };

      function config() {
        return modules
          .reduce(function (d, c) { d[c.id] = c.values; return d; }, {});
      }

      function open() {
        if (this.files[0]) {
          var reader = new FileReader();

          reader.addEventListener('load', function () {
            var data = JSON.parse(reader.result);

            modules.forEach(function (module) {
              if (data[module.id]) { module.merge(data[module.id]); }
            });

            scope.$$phase || scope.$digest();
          });

          reader.readAsText(this.files[0], 'utf-8');
        }
      }
    }
  ]).

  filter('titlecase', function () {
    return function (input) {
      return input
        .replace(/_/g, ' ')
        .replace(/\b[a-z]/g, function (m) { return m[0].toUpperCase(); });
    };
  }).

  directive('setting', [
    function () {
      var TYPES = { number: 'number' };

      return {
        restrict: 'A',
        templateUrl: 'setting.html',
        scope: {
          setting: '=',
          config: '='
        },
        link: function (scope) {

          scope.id      = [scope.config.id, scope.setting.name].join('_');
          scope.type    = TYPES[scope.setting.type] || 'text';
          scope.element = scope.setting.element;

          scope.$watch('setting.get()', function (value) {
            scope.value = value;
          });
        }
      };
    }
  ]).

  directive('settingInput', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-input.html'
    };
  }]).

  directive('settingCheckbox', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-checkbox.html'
    };
  }]).

  directive('settingSelect', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-select.html'
    };
  }]).

  directive('settingSelect2', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-select2.html'
    };
  }]).

  directive('settingMeteo', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-meteo.html',
      link: function (scope) {
        scope.add = function (value) {
          if (!value.length || value[value.length - 1].name)
            value.push({ name: '', display: '', unit: '',
                         convert: 'none', min: '', max: '', log: false, color: '#000' });
        };
      }
    };
  }]).

  directive('settingMeteogroup', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-meteogroup.html',
      link: function (scope) {
        scope.add = function (value) {
          if (!value.length || value[value.length - 1].name)
            value.push({ name: '', display: '', unit: '', group: '',
                         min: '', max: '', log: false });
        };
      }
    };
  }]).

  directive('settingPro', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-pro.html',
      link: function (scope) {
        scope.version = '0'; // index 0 is legacy, index 1 is version 1.4

        scope.add = function (value) {
          if (!value.length || value[value.length - 1].name)
            value.push({ code: '', name: '', editable: true });
        };

        scope.remove = function (index, value) {
          delete niviz.Feature.type[value[index].name];
          niviz.SimpleProfile.remove(value[index].name);
          niviz.Timeline.remove(value[index].name);
          value.splice(index, 1);
        };
      }
    };
  }]).

  directive('settingBarparams', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-barparams.html',
      link: function (scope) {
        scope.types = niviz.Feature.types;

        scope.update = function () {
          niviz.ProParser.getcodes();
          scope.types = niviz.Feature.types;
        };

        scope.add = function (value) {
          if (!value.length || value[value.length - 1].name)
            value.push({ name: '', style: 'line', color: '#000000', log: false });
        };
      }
    };
  }]).

  directive('settingTimelineparams', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-timelineparams.html',
      link: function (scope) {
        scope.types = niviz.Feature.types;

        scope.update = function () {
          niviz.ProParser.getcodes();
          scope.types = niviz.Feature.types;
        };

        scope.add = function (value) {
          if (!value.length || value[value.length - 1].name)
            value.push({ name: '', min: '', max: '', steps: '', palette: 'whiteblue', editable: true });
        };
      }
    };
  }]).

  directive('settingColorpicker', [function () {
    return {
      restrict: 'E', scope: false, replace: true, templateUrl: 'setting-colorpicker.html'
    };
  }]);

}(angular, jQuery));
