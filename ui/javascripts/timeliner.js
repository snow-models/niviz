/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $, moment) {
  'use strict';

  angular
    .module('niviz.timeliner', [
      'niviz.lib',
      'niviz.util',
      'niviz.console',
      'niviz.settings'
    ])

    .directive('timeliner', [
      'niviz',
      'print',
      'registry',
      'open',
      '$timeout',
      '$location',
      '$console',
      '$rootScope',
      'timeline-hs-settings',
      'extract-time-series',

      function (niviz, print, registry, open, timeout, location, $console, $root, thss, ets) {
        return {
          restrict: 'A',
          templateUrl: 'timeliner.html',
          scope: { timeline: '=', close: '&onClose', width: '=', lang: '=', hscale: '=' },
          link: function (scope, element, attrs) {
            window.console.dir('Setting up timeliner controller');
            var W = $(window), doc = $(document),
              height = element.parent().height(),
              width  = W.width(), maxwidth = scope.width || 1000,
              canvas = $('#canvas', element),
              date = moment(location.search().date, 'YYYYMMDDTHHmm'),
              hashcode = new Date().getTime();

            scope.snow_height_settings = function () {
              thss(scope.graph && scope.graph.properties).then(function (data) {
                if (scope.graph && data) {
                  scope.graph.setProperties({ hsmin: data.min, hsmax: data.max });
                  scope.graph.draw();
                }
              });
            };

            scope.extract = function () {
              ets(scope.graph).then(function (d) {
                if (scope.graph && d) {
                  scope.$parent.$parent.extracted.meteo = null;
                  timeout(function () {
                    scope.$parent.$parent.extracted = {
                      meteo: scope.graph.data.extract(d.parameter.lookup, d.height, d.isdepth, d.functor)
                    }

                    scope.$parent.$parent.overlap();
                  }, 50);
                }
              });
            };

            scope.print = function () {
              print(canvas, scope.station && scope.station.filename || null);
            };

            var input = $('<input type="file" />');

            input.on('change', function () {
              var self = this;
              scope.processing = true;
              scope.$$phase || scope.$digest();

              open.loadfile(this.files[0], function (error, meteo) {
                var fn = self.files[0].name;
                if (error) {
                  $console.error('While trying to parse ' + fn + ': ' + error.message);
                } else if (niviz.Meteo.prototype.isPrototypeOf(meteo)) {
                  scope.graph.data.meteo = meteo;
                } else {
                  $console.error('Not time series data: ' + fn);
                }

                scope.processing = false;
                scope.$$phase || scope.$digest();
                $root.$$phase || $root.$digest();
                self.value = null;
              });
            });

            scope.opentimeseries = function () { input[0].click(); };

            scope.save = function (type) {
              var name = scope.graph.data.name + '.timeline';
              scope.$parent.save(type, scope.graph, name);
            };

            scope.properties = function (parameter) {
              if (scope.graph && scope.graph.properties.parameter === parameter) return;
              scope.processing = true;
              timeout(function () {
                scope.graph.setProperties({'parameter': parameter});
                scope.graph.draw();
                scope.processing = false;
                scope.$parent.$parent.activeclass = parameter;
              }, 10);
            };


            // Deal with showing / hiding soil layers
            scope.soil = true;        // whether soil is shown
            scope.togglesoil = false; // whether soil present in the station object

            scope.showsoil = function (enable) {
              scope.processing = true;
              scope.soil = enable;
              timeout(function () {
                if (scope.station) {
                  scope.station.showsoil(enable);
                  scope.station.emitter.emit('profile', scope.station.current);
                }
                if (scope.graph) scope.graph.draw();
                scope.processing = false;
              }, 10);
            };

            function update () {
              if (!scope.station) return;
              height = element.parent().height();

              if (scope.hscale === 1) {
                element.height(height - 10);
              } else {
                element.height(height / (scope.hscale || 1) - (scope.hscale || 1 - 1) * 4);
              }

              element.width(Math.min(Math.max(width, maxwidth), maxwidth));
              canvas.height(element.height() - 50);
              canvas.width(element.width() - 20);

              //setTimeout(function () {// HACK: otherwise wrong dimensions are passed to graph
                if (!scope.graph) {
                  scope.graph = niviz.draw('Timeline', scope.station, canvas, {
                    parameter: 'grainshape'
                  });

                  if (date.isValid()) scope.graph.current(date);
                  registry.add(hashcode, scope.graph);
                } else {
                  scope.graph.draw();
                }

                scope.parameters = scope.graph.parameters;
              //}, 10);
            }

            var keyhandler = function (e) {
              if (e.keyCode === 39) {
                scope.graph.next();
              } else if (e.keyCode === 37) {
                scope.graph.previous();
              }
            };

            doc.on('keydown', keyhandler);

            scope.$on('$destroy', function () {
              console.log('destruction of timeliner');
              doc.off('keydown', keyhandler);
              registry.remove(hashcode);
            });

            // The following $watch is there for the simple.html view only:
            scope.$watch('$parent.$parent.$parent.activeclass', function (p) {
              if (scope.$parent.$parent.simplemode) scope.properties(p);
            });

            scope.$watch('width', function (w) {
              maxwidth = scope.width || 1000;
              update();
            });

            scope.$watch('$parent.height + hscale', function (h) {
              update();
            });

            scope.$on('settings', function (e, c) {
              update();
            });

            scope.$watch('timeline', function (station) {
              if (station) {
                scope.station = station;
                if (station.bottom < 0) scope.togglesoil = true;
                update();
              }
            });
          }
        };
      }
    ]);

}(angular, jQuery, moment));
