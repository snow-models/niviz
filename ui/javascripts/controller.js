/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  angular
    .module('niviz.controller', [
      'niviz.lib',
      'niviz.util',
      'niviz.console'
    ])

    .directive("dropzone", function() {
      return {
          restrict : "A",
          link: function (scope, elem) {

            elem.bind('dragover', function(e) {
              e.preventDefault();
            });

            elem.bind('drop', function(e) {
              e.stopPropagation();
              e.preventDefault();

              var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer;
              var files = dataTransfer.files;

              if (files && files.length >= 1) {
                scope.openFiles(files, function () {}); // parent scope is NivizController
              }
            });
          }
      }
    })

    .controller('NivizController', [
      '$scope',
      '$state',
      '$rootScope',
      'niviz',
      '$console',
      '$location',
      '$http',
      '$window',
      'save',
      'registry',
      'open',

      function (scope, $state, $root, niviz, console, location, http, $window, save, registry, open) {
        var t = niviz.Translate.gettext;

        scope.niviz = niviz;
        scope.simplemode = false;
        scope.activeclass = 'grainshape';
        scope.lang = niviz.Common.defaults.language;
        scope.synchronized = false;

        scope.meteos = [];
        scope.profiles = [];
        scope.timelines = [];

        scope.destroy = function (array, index) {
          scope[array].splice(index, 1);
        };

        scope.deregister = function (timeline) {
          timeline.meteo = null;
        };

        function check_caaml5 (graph) {
          if (!graph.station || !graph.profile) return;

          var showalert = false;

          ['ct', 'ect', 'rb', 'sf', 'saw'].forEach(function (p) {
            if (graph.profile[p] && graph.profile[p].elements.length > 1)
              showalert = true;
          });

          if (showalert) alert(t('_warning_caaml5_stability_tests', 'ui'));
        }

        scope.save = function (type, graph, name) {
          if (!type || !graph || !name) return;

          try {
            niviz.convert(type, graph, function (data, error) {
              var mime;

              if (type === 'SVG') {
                name = name + '.svg';
                mime = 'image/svg+xml';
              } else if (type === 'PNG') {
                name = name + '.png';
                mime = 'image/png';
              } else if (type === 'JSON') {
                name = name + '.json';
                mime = 'application/json';
              } else if (type === 'CSV') {
                name = name + '.csv';
                mime = 'text/csv';
              } else if (type === 'CAAML') {
                name = name + '.caaml';
                mime = 'text/xml';
              } else if (type === 'CAAML5') {
                name = name + '-v5.caaml';
                mime = 'text/xml';
                check_caaml5(graph);
              }

              save(name, mime, data);
            });
          } catch (e) {
            console.error('Error while trying to save file: ' + e.message);
            throw e;
          }
        };

        scope.rearrange = function () {
          scope.$root.$broadcast('rearrange');
        };

        scope.synchronize = function () {
          registry.synchronize();
          scope.synchronized = true;
        };

        scope.desynchronize = function () {
          registry.desynchronizeAll();
          scope.synchronized = false;
        };

        scope.newprofile = function () {
          var station = new niviz.Station();
          var profile = new niviz.Profile(moment());

          station.name = 'StationName';
          station.height = profile.hs = profile.ph = 200;
          profile.top = 200;
          profile.bottom = 0;
          station.push(profile);
          scope.profiles.push(station);
        };

        function parse (data, type, filename, callback) {
          try {
            niviz.parse(data, type, function (error, station) {
              if  (error) {
                console.error('Error while parsing "' + filename + '": ' + error.message);
                delete scope.parsing;
                scope.$$phase || scope.$digest();
                callback('error');
              } else {
                station.filename = filename;
                callback(station);
              }
            });
          } catch (e) {
            console.error('While trying to parse ' + filename + ': ' + e.message);
            delete scope.parsing;
            scope.$$phase || scope.$digest();
            callback('error');
          }
        }

        function display (station, showspinner) {
          if (niviz.Meteo.prototype.isPrototypeOf(station)) {
            scope.meteos.push(station);
          } else {
            if (station.profiles.length === 1) {
              scope.profiles.push(station);
            } else if (station.profiles.length > 1) {
              scope.timelines.push(station);
            }
          }

          if (!showspinner) {
            delete scope.parsing;
            scope.$$phase || scope.$digest();
          }
        }

        function loadfiles (files, index, station, callback) {
          scope.parsing = true;
          scope.$$phase || scope.$digest();

          if (index >= files.length) { // all files processed
            display(station);
            callback();
          } else {
            open.loadfile(files[index], function (error, current) {

              if (error) {
                scope.parsing = false;
                console.error('While trying to parse ' + files[index].name + ': ' + error.message);
                scope.$$phase || scope.$digest();

                callback();
              } else if (station === null) {
                loadfiles(files, index + 1, current, callback);
              } else {
                if (station.merge && current.profiles) {
                  station.merge(current);
                } else {
                  display(current);
                }

                loadfiles(files, index + 1, station, callback);
              }
            });
          }
        }

        var input = $('<input type="file" multiple="multiple"/>');

        input.on('change', function () {
          var self = this;
          scope.openFiles(this.files, function () {
            self.value = null;
          });
        });

        scope.openFiles = function (files, callback) {
          var self = this;
          if (files.length >= 1) {
            loadfiles(files, 0, null, callback);
          }
        };

        scope.open = function () { input[0].click(); };

        function loadurl (file, callback) {
          var isuri = file.match(/\//g), isbasicauth = file.match(/@/g),
              authorization, filename = file,
              urlRegex = /\(?(?:(http|https|ftp):\/\/)?(?:((?:[^\W\s]|\.|-|[:]{1})+)@{1})?((?:www.)?(?:[^\W\s]|\.|-)+[\.][^\W\s]{2,4}|localhost(?=\/)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::(\d*))?([\/]?[^\s\?]*[\/]{1})*(?:\/?([^\s\n\?\[\]\{\}\#]*(?:(?=\.)){1}|[^\s\n\?\[\]\{\}\.\#]*)?([\.]{1}[^\s\?\#]*)?)?(?:\?{1}([^\s\n\#\[\]]*))?([\#][^\s\n]*)?\)?/gi;

          if (isbasicauth) {
            var authtoken = file.replace(urlRegex, '$2');
            filename = file.replace(urlRegex, '$6') + file.replace(urlRegex, '$7');
            authorization = { 'Authorization': 'Basic ' + btoa(authtoken) };
          }

          var fnext = open.extname(file);
          var xhrOverride = new XMLHttpRequest();
          if (fnext === 'gz') xhrOverride.responseType = 'arraybuffer';

          $.ajax({
            type: 'GET',
            url: file,
            headers: authorization,
            crossDomain: true,
            xhr: function() { return xhrOverride; }
          }).done(function (response) {
            //console.info('Downloaded file "%s"', file);
            var result = response;

            if (fnext === 'gz') {
              result = pako.inflate(response, { to: 'string' });
              fnext = open.extname(filename.replace('.' + fnext, ''));
            }

            niviz.parse(result, fnext, function (error, station) {
              if (!error) {
                station.filename = filename;
                display(station, true);
              } else {
                console.error('While trying to parse ' + filename + ': ' + error.message);
              }

              callback();
            });
          }).fail(function (jqXHR, textStatus, errorThrown) {
            callback();
            var text = $(jqXHR.responseText).text()
                || (jqXHR.status + ' - ' + jqXHR.statusText);
            console.error(filename + ': ' + text);
            scope.$$phase || scope.$digest();
          });
        }

        // http file loader
        function init (file) {
          if (!file) return;
          var res = file.split(','), returns = 0;

          scope.parsing = true;

          function spinning () {
            returns++;

            if (returns === res.length) {
              delete scope.parsing;
              scope.$$phase || scope.$digest();
            }
          }

          res.forEach(function (url) {
            loadurl(url, spinning);
          });

        }

        scope.$on('jsonfile', function (e, c) {
          scope.parsing = true;

          niviz.parse(JSON.stringify(c), 'json', function (error, station) {

            scope.parsing = false;

            if (!error) {
              station.filename = station.filename || station.id;
              display(station, true);
            } else {
              console.error('While trying to parse local JSON: ' + error.message);
            }

          });
        });

        scope.$on('settings', function (e, c) {
          scope.lang = niviz.Common.defaults.language;
        });

        $root.$on('$stateChangeSuccess', function(e, state, params) {
          if (state.name === 'main' || state.name === 'main2' || state.name === 'simple'
              || state.name === 'full')
            init(location.search().file || location.search().url);
        });

        scope.go = function (state, params, options) {
          $state.go(state, params, options);
        };

        scope.$on('$destroy', function () {
          angular.element($window).off('resize', onResize);
        });

        function onResize() {
          scope.$root.$broadcast('rearrange');
        }

        angular.element($window).on('resize', onResize);
      }
    ]);

}(angular, jQuery));
